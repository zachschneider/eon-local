<?php
define( 'WP_CACHE', true );    // Added by WP Rocket.
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'q8)G#(%:6#ldbApl=r)pr&g>Xvny7-BvIvLJd[Ck&8F$y7c[L@Bie/ZrwkU6lPfL' );
define( 'SECURE_AUTH_KEY',   '9j+~ (3XrLRE0YCR&AKE/9g=HUb`oqH@~Dg0W3xO[.X^5w8>3Y@8D,z?9I*i:[n:' );
define( 'LOGGED_IN_KEY',     'yp1;d6]&%K,<3n$5o5+R@w].MdWqNGRi+4}YLEA4aH`W@=Zob!Cwz(t@My5_u<ls' );
define( 'NONCE_KEY',         ',-E7nyhccrQhJ%B{v^uV_h*GcJYc}y(z)#*U2]a.f_~faiUQe)O1IDG057@TWKW@' );
define( 'AUTH_SALT',         '?uA*eKJ[c?9LgS:;y9?O^tV@Lz&wI:_mz*$18w;GIj?dX~?;5~%V:v!Na:|7z$+]' );
define( 'SECURE_AUTH_SALT',  'xK&g[b[@end/.9;qARM5nvdAYkItmKQP:T}_#):*y-?-%i^OSXBGHKBPIhE{ebf(' );
define( 'LOGGED_IN_SALT',    '}vsw~9l=uj6ZaoPq)u`QSmCw(^jQHR:Ku-%4$B%-rxjm7Jl@=;36sE,u?Cxs(4pd' );
define( 'NONCE_SALT',        '9*&2pvH[LD&ajy!hIR-[e&__Uu9pvia|IWA~RJ*&m<qDM=o;^tO9{szG~4]+MjH6' );
define( 'WP_CACHE_KEY_SALT', 'sTJ6kw72Dhykd@;|6[>E4XzsZj`UNjrdWN5);a0v=^,DR2(aj~;WM/kp1}e&.pSn' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'JETPACK_DEV_DEBUG', True );
define( 'WP_DEBUG', True );
define( 'FORCE_SSL_ADMIN', False );
define( 'SAVEQUERIES', False );

// Additional PHP code in the wp-config.php
// These lines are inserted by VCCW.
// You can place additional PHP code here!


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
