$(document).ready(function () {

  $('.hero-jumpto').on('click', function(evt) {
    evt.preventDefault();
    let offsetTop = $("#home-payment-opts").offset().top;

    try {
      window.scrollTo({
        top: offsetTop,
        left: 0,
        behavior: 'smooth'
      });
    } catch(e) {
      window.scrollTo(offsetTop, 0);
    }
  });

  // slick slider
  $('.carousel').slick({
    dots: true,
    appendArrows: $('.carousel-nav')
  });
});