<?php get_header(); ?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
				<H1> Oops! We can't find this page.</H1>
					<p> Use the navigation above or try these helpful links instead:
					</p>
				<body>
					<p>
						
					</p>
					<li><a href="https://www.eonclinics.com/what-are-dental-implants">What is a Dental Implant?</a></li>
					<li><a href="https://www.eonclinics.com/dental-implant-benefits">What are the Benefits of Dental Implants?</a></li>
					<li><a href="https://www.eonclinics.com/hear-our-patients-stories">Watch Our Patient Testimonial Videos</a></li>
					<li><a href="https://www.eonclinics.com/sitemap">View Sitemap</a></li>
				</body>
				 <!-- .et_pb_post -->
			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php

get_footer();
