<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>
				<div class="container" id="footer-logo-div">
					<img id="footer-eon-logo" src="https://www.eonclinics.com/wp-content/uploads/2019/12/EON-Logo_Wh.png" />
				</div>
				<div class="container" id="footer-text-under-logo">
					<p>EON Clinics Dental Implant Centers in Illinois, Indiana, and Wisconsin are family-owned and operated. As a result, we are able to reinvest profits in what matters to us most ⁠— providing exceptional patient care with a fair cost of dental implants and supporting the communities our employees and patients live in. The majority of our patients qualify for same day dental implants performed by our highly-skilled dental implant doctors, including prosthodontists, periodontists, and oral surgeons. Dental implants can last a lifetime. Studies have shown that in most cases, dental implants can last longer than 25 years with proper care and regular cleanings.
					</p>
				</div>

				<div id="footer-bottom">
					<div class="container clearfix">
				<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}

					echo et_get_footer_credits();
				?>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
	<!-- Divi Script - Open Social Media icons in a new tab -->
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".et-social-icon a").attr('target', 'blank');
	});
	</script>
</body>
</html>