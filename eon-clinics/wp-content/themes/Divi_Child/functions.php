<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );}
	
/**
 * Load the Optimizely X script *after jQuery*
 */
function init_optimizely_after_jquery() {
    wp_enqueue_script( 'optimizely-x', '//cdn.optimizely.com/js/16342820700.js', array( 'jquery' ), '', false );
}
add_action( 'wp_enqueue_scripts', 'init_optimizely_after_jquery' );

//======================================================================
// CUSTOM DASHBOARD
//======================================================================
// ADMIN FOOTER TEXT
function remove_footer_admin () {
    echo "Divi Child Theme";
} 

add_filter('admin_footer_text', 'remove_footer_admin');

/* Gravity Forms Read Only Class */

// update '1' to the ID of your form
add_filter( 'gform_pre_render_5', 'add_readonly_script' );
function add_readonly_script( $form ) {
    ?>
 
    <script type="text/javascript">
        jQuery(document).ready(function(){
            /* apply only to a input with a class of gf_readonly */
            jQuery("li.gf_readonly input").attr("readonly","readonly");
        });
    </script>
 
    <?php
    return $form;
}



// CUSTOM POST TYPE - Doctors
function create_posttype() {
	register_post_type( 'doctors',
    array(
      'labels' => array(
        'name' => __( 'Doctors' ),
        'singular_name' => __( 'Doctor' )
      ),
      'public' => true,
      'has_archive' => false,
      'rewrite' => array('slug' => 'doctors','with_front' => false),
      'taxonomies' => array( 'category' ),
      'menu_position' => 5,
      'supports' => array(
          'title',
          'thumbnail',
					'editor',
					'excerpt' )
    )
  );
	}
add_action( 'init', 'create_posttype' );

flush_rewrite_rules( false );

// END
