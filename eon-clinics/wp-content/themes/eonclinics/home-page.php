<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package eonclinics
 * 
 */
/*
Template Name: Home Page
Template Post Type: page
*/
get_header();
?>
<?php $image = wp_get_attachment_image_src(get_field('affordable_hero_point_one'), 'full'); ?>
<?php $heroIcon2 = wp_get_attachment_image_src(get_field('affordable_hero_icon_2'), 'full'); ?>
<?php $heroIcon3 = wp_get_attachment_image_src(get_field('affordable_hero_icon_3'), 'full'); ?>
<?php $paymentLocationsImg = wp_get_attachment_image_src(get_field('affordable_locations_section_image'), 'full'); ?>



<section id="intro">
  <div style="background-image: url(<?php the_field('affordable_hero_image'); ?>)">
    <h1><span><?php the_field('affordable_hero_sub_header_text'); ?></span><?php the_field('affordable_hero_header_text'); ?></h1>
    <div class="more-info">
      <div>
        <div class="payments info-txt">
          <a href="#" class="hero-jumpto" rel="nofollow">
            <span class="icon icon-calendar">

            <?php
              $svg = wp_remote_get($image[0])['body'];
              $dom = new DOMDocument();
              $dom->loadHTML($svg);
              foreach($dom->getElementsByTagName('svg') as $element) {
                  $element->setAttribute('class','icon icon-calendar');  
              }
              $dom->saveHTML();
              $svg = $dom->saveHTML();
            ?>
              <?php echo $svg ?>
            <span><?php the_field('affordable_hero_icon_1_copy'); ?></span>
          </a>
        </div>
        <div class="financing info-txt">
          <a href="#" class="hero-jumpto" rel="nofollow">
            <?php
                $svg2 = wp_remote_get($heroIcon2[0])['body'];
                $dom = new DOMDocument();
                $dom->loadHTML($svg2);
                foreach($dom->getElementsByTagName('svg') as $element) {
                    $element->setAttribute('class','icon icon-dots');  
                }
                $dom->saveHTML();
                $svg2 = $dom->saveHTML();
              ?>
              <?php echo $svg2 ?>
            <span><?php the_field('affordable_hero_icon_2_copy'); ?></span>
          </a>
        </div>
        <div class="zero-interest info-txt">
          <a href="#" class="hero-jumpto" rel="nofollow">
            <?php
                $svg3 = wp_remote_get($heroIcon3[0])['body'];
                $dom = new DOMDocument();
                $dom->loadHTML($svg3);
                foreach($dom->getElementsByTagName('svg') as $element) {
                    $element->setAttribute('class','icon icon-zero');  
                }
                $dom->saveHTML();
                $svg3 = $dom->saveHTML();
              ?>
              <?php echo $svg3 ?>
            <span><?php the_field('affordable_hero_icon_3_copy'); ?></span>
          </a>
        </div>
      </div>
      <a href="#" class="icon-chevron floating hero-jumpto" rel="nofollow"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 16"><g id="Path_41" data-name="Path 41"><path d="M15.5,15.5.9,2.56A1.12,1.12,0,0,1,.9.85a1.5,1.5,0,0,1,1.93,0L15.5,12.08,28.17.85a1.5,1.5,0,0,1,1.93,0,1.12,1.12,0,0,1,0,1.71Z"/></g></svg></a>
    </div>
  </div>
</section>
<section id="home-payment-opts">
  <div class="section-container">
    <div class="plans-offered">
      <div class="mobile-side-padding tablet-side-padding max-930 center-elem">
        <header>
          <p class="sect-hdr-txt primary-blue-txt"><?php the_field('affordable_payment_plans_header_1'); ?></p>
          <p class="sect-hdr-txt primary-blue-txt"><?php the_field('affordable_payment_plans_header_2'); ?></p>
        </header>
        <p><?php the_field('affordable_payment_plans_header_paragraph'); ?></p>
      </div>
    </div>
    <div class="accept-payments laptop-side-padding">
      <div class="eon-front-entrance">
        <img src="<?php the_field('affordable_locations_section_image'); ?>" class="full-width" />
      </div>
      <div class="payment-opts">
        <div class="mobile-side-padding tablet-side-padding">
          <h1 class="sect-hdr-txt primary-blue-txt"><?php the_field('affordable_payment_location_section_header'); ?></h1>
          <?php the_field('affordable_payment_locations_post-list_paragraph'); ?>

          <?php 
          $link = get_field('affordable_payment_locations_final_link_cta');
          if( $link ): 
              $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
              ?>
              <a class="cta" href="<?php echo esc_url( $link_url ); ?>"><?php echo esc_html( $link_title ); ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="home-lifetime-implants" class="primary-blue-bg">
  <div class="section-container mobile-side-padding laptop-side-padding">
    <header class="center-elem">
      <p class="sect-hdr-txt"><?php the_field('affordable_implants_last_a_lifetime_header_line_1'); ?></p>
      <p class="sect-hdr-txt"><?php the_field('affordable_implants_last_a_lifetime_header_line_2'); ?></p>
    </header>
    <div class="implant-benefits">
      <?php if( have_rows('affordable_benefits_last_lifetime') ): ?>
        <?php while( have_rows('affordable_benefits_last_lifetime') ): the_row(); 
          // vars
          $iconImg = wp_get_attachment_image_src(get_sub_field('lifetime_implants_icons'), 'full');
          $iconTitle = get_sub_field('lifetime_implants_icon_title');
          $iconCopy = get_sub_field('lifetime_implants_icon_copy');
          ?>


        <div class="benefit">
          <?php
          $svg5 = wp_remote_get($iconImg[0])['body'];
          $dom = new DOMDocument();
          $dom->loadHTML($svg5);
          foreach($dom->getElementsByTagName('svg') as $element) {
              $element->setAttribute('class','icon');  
          }
          $dom->saveHTML();
          $svg5 = $dom->saveHTML();
          ?>
          <?php echo $svg5; ?>
          <h2 class="sect-hdr2-txt"><?php echo $iconTitle; ?></h2>
          <p><?php echo $iconCopy; ?></p>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
  </div>
</section>
<section id="home-personal-treatments">
  <div class="section-container laptop-side-padding">
    <div class="treatment-visuals">
      <div class="carousel-nav">
        <svg class="carousel-next" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 16"><g id="Path_41" data-name="Path 41"><path d="M15.5,15.5.9,2.56A1.12,1.12,0,0,1,.9.85a1.5,1.5,0,0,1,1.93,0L15.5,12.08,28.17.85a1.5,1.5,0,0,1,1.93,0,1.12,1.12,0,0,1,0,1.71Z"/></g></svg>
        <svg class="carousel-prev" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 16"><g id="Path_41" data-name="Path 41"><path d="M15.5,15.5.9,2.56A1.12,1.12,0,0,1,.9.85a1.5,1.5,0,0,1,1.93,0L15.5,12.08,28.17.85a1.5,1.5,0,0,1,1.93,0,1.12,1.12,0,0,1,0,1.71Z"/></g></svg>
      </div>
      <div class="carousel">
      <?php if( have_rows('affordable_carousel_slider') ): ?>
        <?php while( have_rows('affordable_carousel_slider') ): the_row(); 
          // vars
          $carouselMobileImg = wp_get_attachment_image_src(get_sub_field('affordadable_carousel_image_mobile'), 'full');
          $carouselDesktopImg = wp_get_attachment_image_src(get_sub_field('affordable_carousel_image_desktop'), 'full');
          $carouselImgTitle = get_sub_field('affordable_carousel_implant_type');
          $carouselImgCopy = get_sub_field('affordable_carousel_implant_description');
          ?>


        <div>
          <figure>
            <img src="<?php echo $carouselMobileImg[0] ?>" alt="" class="mobile-img" />
            <img src="<?php echo $carouselDesktopImg[0] ?>" alt="" class="laptop-img" />
            <h2 class="sect-hdr2-txt"><?php echo $carouselImgTitle; ?></h2>
            <p><?php echo $carouselImgCopy; ?></p>
          </figure>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="treatment-cost mobile-side-padding tablet-side-padding">
      <header>
        <h1 class="sect-hdr-txt primary-blue-txt"><?php the_field('personalized_treatment_header'); ?></h1>
        <p><?php the_field('personalized_treatment_header_paragraph'); ?></p>
      </header>
      <table class="procedure-costs">
        <tr>
          <th><?php the_field('procedure_type_header'); ?></th>
          <th><?php the_field('average_cost_header'); ?></th>
        </tr>

        <?php if( have_rows('procedure_type_table') ): ?>
        <?php while( have_rows('procedure_type_table') ): the_row(); 
          // vars
          $procedureTypeHeader = get_sub_field('procedure_type_cell_header');
          $procedureCostHeader = get_sub_field('procedure_cost_cell_header');
          $procedureTypeName = get_sub_field('procedure_type_name');
          $procedureCost = get_sub_field('procedure_type_cost');
          ?>

          <tr>
            <td>
              <span class="cell-label"><?php echo $procedureTypeHeader; ?></span>
              <span class="procedure-type"><?php echo $procedureTypeName; ?></span>
            </td>
            <td>
              <span class="cell-label"><?php echo $procedureCostHeader; ?></span>
              <span class="procedure-cost"><?php echo $procedureCost; ?></span>
            </td>
          </tr>
        <?php endwhile; ?>
        <?php endif; ?>
      </table>

      <p><?php the_field('affordable_post_procedure_table_paragraph'); ?></p>

      <?php 
          $link2 = get_field('affordable_personalized_treatment_cta_button');
          if( $link2 ): 
              $link_url2 = $link2['url'];
              $link_title2 = $link2['title'];
              $link_target2 = $link2['target'] ? $link2['target'] : '_self';
              ?>
              <a class="btn primary" href="<?php echo esc_url( $link_url2 ); ?>"><?php echo esc_html( $link_title2 ); ?></a>
        <?php endif; ?>
    </div>
  </div>
</section>
<section id="home-testimonial"  class="primary-blue-bg">
  <div class="testimonial max-930 center-elem">
    <div class="section-container mobile-side-padding">
      <div class="quote">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 34 27" style="enable-background:new 0 0 34 27;" xml:space="preserve">
        <g class="st0">
          <path d="M12,15.5c1.1,1.2,1.7,2.7,1.7,4.6c0,2-0.6,3.6-1.8,4.9c-1.2,1.2-2.7,1.9-4.6,1.9c-2.1,0-3.7-0.8-5-2.3
            c-1.2-1.5-1.9-3.9-1.9-7.1c0-6.2,2-11.5,6.1-16c0.8-0.8,1.7-1.2,2.8-1.2c0.9,0,1.6,0.3,2.3,0.9c0.6,0.6,1,1.3,1,2.1
            c0,0.9-0.3,1.6-1,2.3c-2.1,2.2-3.4,4.8-4,7.9C9.4,13.6,10.8,14.3,12,15.5z M31.9,15.5c1.1,1.2,1.7,2.7,1.7,4.6c0,2-0.6,3.6-1.8,4.9
            c-1.2,1.2-2.7,1.9-4.6,1.9c-2.1,0-3.7-0.8-5-2.3c-1.2-1.5-1.9-3.9-1.9-7.1c0-6.2,2-11.5,6.1-16c0.8-0.8,1.7-1.2,2.8-1.2
            c0.9,0,1.6,0.3,2.3,0.9c0.6,0.6,1,1.3,1,2.1c0,0.9-0.3,1.6-1,2.3c-2.1,2.2-3.4,4.8-4,7.9C29.3,13.6,30.8,14.3,31.9,15.5z"/>
        </g>
        </svg>
      </div>
      <blockquote><?php the_field('affordable_youtube_quote') ?></blockquote>
    </div>
  </div>
  <div class="testimonial-video">
    <div class="section-container mobile-side-padding laptop-side-padding">
      <div class="videoWrapper">
        <div class="youtube-player" data-id="<?php the_field('affordable_youtube_player_id'); ?>"></div>
      </div>
    </div>
  </div>
</section>
<section id="home-patient-info">
  <div class="section-container mobile-side-padding laptop-side-padding">
    <div>
      <h1 class="sect-hdr-txt"><?php the_field('affordadable_helpful_info_header') ?></h1>
      <div class="helpful-ctas">
      <?php if( have_rows('helpful_information_links') ): ?>
        <?php while( have_rows('helpful_information_links') ): the_row(); 
          // vars
          $helpfulLink = get_sub_field('helpful_link');
          if( $helpfulLink ) {
            $helpfulLink_url = $helpfulLink['url'];
            $helpfulLink_title = $helpfulLink['title'];
            $helpfulLink_target = $helpfulLink['target'] ? $helpfulLink['target'] : '_self';
          }

          $helpfulImg = get_sub_field('helpful_link_image');
          $helpfulCopy = get_sub_field('helpful_link_copy');
          ?>

          
        <div>
          <a href="<?php echo $helpfulLink_url; ?>">
            <img src="<?php echo $helpfulImg; ?>" class="full-width" />
            <p><?php echo $helpfulCopy ?></p>
          </a>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>

      </div>
    </div>
  </div>
</section>

<section id="confidence-cta" class="primary-blue-bg pre-footer-cta">
  <div class="section-container mobile-side-padding tablet-side-padding">
    <div class="ready-to-smile pre-footer-content">
    <h1 class="sect-hdr-txt"><?php the_field('prefooter_header') ?></h1>
      <?php 
        $prefooterLink = get_field('prefooter_cta_button');
        if( $prefooterLink ): 
            $prefoot_link_url = $prefooterLink['url'];
            $prefoot_link_title = $prefooterLink['title'];
            $prefoot_link_target = $prefooterLink['target'] ? $prefooterLink['target'] : '_self';
            ?>
            <a class="btn primary" href="<?php echo esc_url( $prefoot_link_url ); ?>"><?php echo esc_html( $prefoot_link_title ); ?></a>
      <?php endif; ?>
    </div>
  </div>
</section>


<?php
get_footer();
?>