<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package eonclinics
 * 
 */
/*
Template Name: Generic Page
Template Post Type: page
*/
get_header();
?>
<div id="generic-page-template">
  <div class="section-container">
    
    <div class="mobile-side-padding laptop-side-padding tablet-side-padding">
    <?php
      while ( have_posts() ) :
      the_post();
      get_template_part( 'template-parts/content', 'page' );

      endwhile; // End of the loop.
    ?>

    </div><!-- #main -->
  </div><!-- #primary -->
</div>
<section id="confidence-cta">
  <div class="section-container mobile-side-padding tablet-side-padding">
    <div class="ready-to-smile">
      <h1 class="sect-hdr-txt">Ready to smile with more confidence?</h1>
      <a href="#" class="btn primary">Schedule a Free Consultation</a>
    </div>
  </div>
</section>

<?php
get_footer();
