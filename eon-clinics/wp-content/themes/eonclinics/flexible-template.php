<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package eonclinics
 * 
 */
/*
Template Name: Flexible Template
Template Post Type: page
*/
get_header();
?>
<div id="generic-page-template">


<?php if( have_rows('flexible_content') ): ?>
    <?php while( have_rows('flexible_content') ): the_row(); ?>

        <?php if( get_row_layout() == 'page_header_with_image' ): ?>
            <header style="background-image: url(<?php the_sub_field('header_background'); ?>);">
              <div class="section-container mobile-side-padding laptop-side-padding">
                <h1><?php the_sub_field('header_main_title'); ?></h1>
                <h2><?php the_sub_field('header_sub_title'); ?></h2>

                <?php if(the_sub_field('header_paragraph') ) : ?>
                  <?php the_sub_field('header_paragraph'); ?>
                <?php endif; ?>

                <img src="<?php the_sub_field('header_image'); ?>" />

              </div>
            </header>
        
        <?php elseif( get_row_layout() == 'two_column' ): ?>
          <?php $columnWidth = get_sub_field_object('column_widths');
          $columnValue = $columnWidth['value'];
          if ($columnValue == '56 33') {
            $columnClass = 'two-third';
          } elseif ($columnValue == '33 56') {
            $columnClass = 'one-third';
          } else {
            $columnClass = 'fifty-fifty';
          }
          ?>
          <div style="background-color: <?php the_sub_field('full_width_background_color'); ?>" class="section-container mobile-side-padding tablet-side-padding laptop-side-padding">
            <?php if( the_sub_field('title') ): ?>
              <div class="title"><?php the_sub_field('title'); ?></div>
            <?php endif; ?>
            <div class="two-col-layout <?php echo $columnClass;  ?>">
              <div class="left-col">
                <?php if( have_rows('two_column_left') ): ?>
                  <?php while( have_rows('two_column_left') ): the_row(); ?>

                    <?php if( get_row_layout() == 'left_column_content_container' ): ?>
                      <?php the_sub_field('left_column_content'); ?>
                    <?php elseif( get_row_layout() == 'left_column_sub_content_container' ): ?>
                      <?php the_sub_field('left_column_subcontent'); ?>
                    <?php elseif( get_row_layout() == 'box_shadow_call_to_action' ): ?>
                      <div class="box-shadow">
                        <?php the_sub_field('box_content'); ?>
                      </div>
                    <?php elseif( get_row_layout() == 'youtube_video' ): ?>
                      <div class='youtube-player' data-id='<?php the_sub_field('youtube_video_id'); ?>'></div>
                    <?php endif; ?>
                  <?php endwhile; ?>
                <?php endif; ?>
              </div>

              <div class="right-col">
                <?php if( have_rows('two_column_right') ): ?>
                  <?php while( have_rows('two_column_right') ): the_row(); ?>
                    <?php if( get_row_layout() == 'right_column_content_container' ): ?>
                      <div class="<?php $shadowOn = get_sub_field_object('add_box_shadow'); $shadowValue = $shadowOn['value'] === 'Yes' ? 'box-shadow' : ''; echo $shadowValue; ?>">
                      <?php the_sub_field('right_column_content'); ?>
                      </div>
                    <?php elseif( get_row_layout() == 'youtube_video' ): ?>
                      <div class='youtube-player' data-id='<?php the_sub_field('youtube_video_id'); ?>'></div>
                    <?php endif; ?>
                  <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
        
        <?php elseif( get_row_layout() == 'three_column' ): ?>
          <div class="section-container mobile-side-padding tablet-side-padding laptop-side-padding">
            <?php the_sub_field('three_column_title'); ?>
            <div class="three-col-layout">
              <div class="col">
                <?php if( have_rows('column_one') ): ?>
                  <?php while( have_rows('column_one') ): the_row(); ?>
                    <?php if( get_row_layout() == 'column_content_container' ): ?>
                      <?php the_sub_field('column_content'); ?>
                    <?php elseif( get_row_layout() == 'outlined_container' ): ?>
                      <div class="outlined-container box-shadow">
                        <div class="outlined-container-icon">
                          <img src="<?php the_sub_field('icon'); ?>" />
                        </div>
                        <div class="outlined-container-content">
                          <?php the_sub_field('description'); ?>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endwhile; ?>
                <?php endif; ?>
              </div>

              <div class="col">
                <?php if( have_rows('column_two') ): ?>
                  <?php while( have_rows('column_two') ): the_row(); ?>
                    <?php if( get_row_layout() == 'column_content_container' ): ?>
                      <?php the_sub_field('column_content'); ?>
                    <?php elseif( get_row_layout() == 'outlined_container' ): ?>
                      <div class="outlined-container box-shadow">
                        <div class="outlined-container-icon">
                          <img src="<?php the_sub_field('icon'); ?>" />
                        </div>
                        <div class="outlined-container-content">
                          <?php the_sub_field('description'); ?>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endwhile; ?>
                <?php endif; ?>
              </div>

              <div class="col">
                <?php if( have_rows('column_three') ): ?>
                  <?php while( have_rows('column_three') ): the_row(); ?>
                    <?php if( get_row_layout() == 'column_content_container' ): ?>
                      <?php the_sub_field('column_content'); ?>
                    <?php elseif( get_row_layout() == 'outlined_container' ): ?>
                      <div class="outlined-container box-shadow">
                        <div class="outlined-container-icon">
                          <img src="<?php the_sub_field('icon'); ?>" />
                        </div>
                        <div class="outlined-container-content">
                          <?php the_sub_field('description'); ?>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>

          </div>


          <?php elseif( get_row_layout() == 'one_column' ): ?>
          <div style="background-color: <?php the_sub_field('full_width_background_color'); ?>" class="section-container mobile-side-padding tablet-side-padding laptop-side-padding one-col-layout">
            <?php if( the_sub_field('title') ): ?>
              <div class="title"><?php the_sub_field('title'); ?></div>
            <?php endif; ?>
            <?php if( have_rows('one_column_container') ): ?>
              <?php while( have_rows('one_column_container') ): the_row(); ?>
                <?php if( get_row_layout() == 'one_column_content_container' ): ?>
                  <div class="center-elem <?php $field = get_sub_field_object('one_column_content_length'); $value = $field['value']; echo explode(":", $value)[0]; ?>"><?php the_sub_field('one_column_content'); ?></div>

                <?php elseif( get_row_layout() == 'outlined_containers' ): ?>
                  <div class="outlined-container box-shadow">
                    <div class="outlined-container-icon">
                      <img src="<?php the_sub_field('icon'); ?>" />
                    </div>
                    <div class="outlined-container-content">
                      <h1><?php the_sub_field('container_title'); ?></h1>
                      <?php the_sub_field('container_description'); ?>
                    </div>
                  </div>
                
                <?php elseif( get_row_layout() == 'faq_container' ): ?>
                  <ul class="accordion faqs">
                  <?php if( have_rows('q_and_a') ): ?>
                    <?php while( have_rows('q_and_a') ): the_row(); 

                      $faqQuestion = get_sub_field('question');
                      $faqAnswer = get_sub_field('answer');
                      ?>

                    <li>
                      <a class="toggle"><?php echo $faqQuestion; ?><span class="icon-chevron"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 16"><g id="Path_41" data-name="Path 41"><path d="M15.5,15.5.9,2.56A1.12,1.12,0,0,1,.9.85a1.5,1.5,0,0,1,1.93,0L15.5,12.08,28.17.85a1.5,1.5,0,0,1,1.93,0,1.12,1.12,0,0,1,0,1.71Z"/></g></svg></span></a>
                      <div class="inner"><?php echo $faqAnswer; ?></div>
                    </li>

                    <?php endwhile; ?>
                    <?php endif; ?>
                    
                  </ul>

                <?php elseif( get_row_layout() == 'two_column_content_container' ): ?>
                  <div class="two-col-layout">
                    <?php if( have_rows('left_column') ): ?>
                    <?php while( have_rows('left_column') ): the_row(); ?>
                      <div class="left-col">

                        <?php if( get_row_layout() == 'youtube_video' ): ?>
                          <div class='youtube-player' data-id='<?php the_sub_field('youtube_id'); ?>'></div>
                        <?php elseif( get_row_layout() == 'general_content_container' ): ?>
                          <?php the_sub_field('general_content'); ?>
                        <?php endif; ?>

                      </div>
                    <?php endwhile; ?>
                    <?php endif; ?>

                    <?php if( have_rows('right_column') ): ?>
                    <?php while( have_rows('right_column') ): the_row(); ?>
                      <div class="right-col">

                        <?php if( get_row_layout() == 'general_content_conatiner' ): ?>
                          <?php the_sub_field('general_content'); ?>'
                        <?php endif; ?>

                      </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                  </div>
                <?php elseif( get_row_layout() == 'color_bar_with_text' ): ?>
                  <div class="headline-colored-car" style="background-color: <?php the_sub_field('background_color'); ?>">
                    <?php the_sub_field('copy'); ?>
                  </div>
                <?php endif; ?>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>

        <?php elseif( get_row_layout() == 'three_column_image_w_more_info_box' ): ?>

          <div class="section-container mobile-side-padding tablet-side-padding laptop-side-padding">
          <div class="title"><?php the_sub_field('title'); ?></div>
          <?php if( have_rows('image_with_title_and_more_info') ): ?>
            <div class="three-col-infinite">
            <?php while( have_rows('image_with_title_and_more_info') ): the_row();?>
              <div class="col">
                <img src="<?php the_sub_field('image'); ?>" />
                <?php the_sub_field('title_and_sub_title'); ?>
                <button class="toggle-more-info"><?php the_sub_field('more_info_button_text'); ?></button>
                <div class="more-info-box"><?php the_sub_field('more_info_box_copy'); ?></div>
              </div>
            <?php endwhile; ?>
            </div>
          <?php endif; ?>
          </div>
        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>
</div>

<?php
get_footer();
?>