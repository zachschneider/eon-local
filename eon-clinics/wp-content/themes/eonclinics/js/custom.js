/**
 * File skip-link-focus-fix.js. Helps with accessibility for keyboard only users. Comes with underscores* Learn more: https://git.io/vWdr2
 */
( function() {
	var isIe = /(trident|msie)/i.test( navigator.userAgent );

	if ( isIe && document.getElementById && window.addEventListener ) {
		window.addEventListener( 'hashchange', function() {
			var id = location.hash.substring( 1 ),
				element;

			if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
				return;
			}

			element = document.getElementById( id );

			if ( element ) {
				if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false );
	}
} )();






// BEGIN LEGACY CUSTOM JS CODE
jQuery(function ($) {
  //open toggle on button click
  $('a.open-toggle').on('click', function(event){
    $('#toggle3.et_pb_toggle_2 .et_pb_toggle_title').click();
  });
});

document.addEventListener('DOMContentLoaded', function() {
  var div,
      n,
      v = document.getElementsByClassName('youtube-player');

  for (n = 0; n < v.length; n++) {
      div = document.createElement('div');
      div.setAttribute('class', 'inner-youtube');
      div.setAttribute('data-id', v[n].dataset.id);
      div.innerHTML = labnolThumb(v[n].dataset.id);
      div.onclick = labnolIframe;
      v[n].appendChild(div);
  }
});

function labnolThumb(id) {
  var thumb = '<img src=\"https://i.ytimg.com/vi/ID/maxresdefault.jpg\">';
  return thumb.replace('ID', id);
}

function labnolIframe() {
  var iframe = document.createElement('iframe');
  iframe.setAttribute('src', 'https://www.youtube.com/embed/' + this.dataset.id + '?');
  iframe.setAttribute('frameborder', '0');
  iframe.setAttribute('allowfullscreen', '1');
  this.parentNode.replaceChild(iframe, this);
}

var handleLoadEvt = function(evt) {
  var elems = document.getElementsByClassName('inner-youtube');

  if (elems && elems.length && elems.length > 0) {
  (function myLoop (i) {
    setTimeout(function () {
      elems[i-1].click();
      if (--i) myLoop(i);
    }, 1)
  })(elems.length);  
  }

  window.removeEventListener('load', handleLoadEvt);
} 

window.addEventListener('load', handleLoadEvt);






// BEGIN CUSTOM THEME CODE
$(document).ready(function () {

  $('.hero-jumpto').on('click', function(evt) {
    evt.preventDefault();
    let offsetTop = $("#home-payment-opts").offset().top;

    try {
      window.scrollTo({
        top: offsetTop,
        left: 0,
        behavior: 'smooth'
      });
    } catch(e) {
      window.scrollTo(offsetTop, 0);
    }
  });

  // slick slider
  $('.carousel').slick({
    dots: true,
    appendArrows: $('.carousel-nav')
  });


  // accordion on home page
  $('.toggle').on('click', function(e) {
  	e.preventDefault();
  
    var $this = $(this);
    if ($this.hasClass('shown')) {
      $this.removeClass('shown');
    } else {
      $this.parent().siblings().find('.shown').removeClass('shown');
      $this.addClass('shown');
    }
  
    if ($this.next().hasClass('show')) {
        $this.removeClass('shown');
        $this.next().removeClass('show');
        $this.next().slideUp(350);
    } else {
        $this.parent().parent().find('li .inner').removeClass('show');
        $this.parent().parent().find('li .inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
    }
  });

  $('button.toggle-more-info').on('click', function(e) {
    var $this = $(this);
    $this.toggleClass('active');
    $this.siblings('.more-info-box').slideToggle(350);
  })
});