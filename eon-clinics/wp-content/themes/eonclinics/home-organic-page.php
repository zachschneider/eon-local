<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package eonclinics
 * 
 */
/*
Template Name: Home Organic Page
Template Post Type: page
*/
get_header();
?>
<?php $image = wp_get_attachment_image_src(get_field('affordable_hero_point_one'), 'full'); ?>
<?php $heroIcon2 = wp_get_attachment_image_src(get_field('affordable_hero_icon_2'), 'full'); ?>
<?php $heroIcon3 = wp_get_attachment_image_src(get_field('affordable_hero_icon_3'), 'full'); ?>
<?php $paymentLocationsImg = wp_get_attachment_image_src(get_field('affordable_locations_section_image'), 'full'); ?>



<section id="intro">
    <?php if( have_rows('organic_hero_image_and_cta_copy') ): ?>
        <?php while( have_rows('organic_hero_image_and_cta_copy') ): the_row();
            ?>

            <div style="background-image: url(<?php the_sub_field('organic_hero_image'); ?>)">
              <h1><span><?php the_sub_field('organic_hero_sub_header_text'); ?></span><?php the_sub_field('organic_hero_header_text'); ?></h1>
              <div class="more-info max-540">
                <p><?php the_sub_field('organic_hero_small_description'); ?></p>
                <a href="#" class="icon-chevron floating hero-jumpto" rel="nofollow"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 16"><g id="Path_41" data-name="Path 41"><path d="M15.5,15.5.9,2.56A1.12,1.12,0,0,1,.9.85a1.5,1.5,0,0,1,1.93,0L15.5,12.08,28.17.85a1.5,1.5,0,0,1,1.93,0,1.12,1.12,0,0,1,0,1.71Z"/></g></svg></a>
              </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
  </section>

  <section id="home-consider-implants">
    <div class="section-container">
      <div class="plans-offered">
        <div class="mobile-side-padding tablet-side-padding max-690 center-elem">
          <?php if( have_rows('consider_implants_but_what_about_cost_section_pre_table') ): ?>
          <?php while( have_rows('consider_implants_but_what_about_cost_section_pre_table') ): the_row();
              ?>
            <h1 class="sect-hdr-txt primary-blue-txt"><?php the_sub_field('organic_what_about_costs_header'); ?></h1>
            <?php the_sub_field('organic_what_about_costs_header_paragraph'); ?>
          <?php endwhile; ?>
      <?php endif; ?>



          
          <table class="procedure-costs">
            <tr>
              <th><?php the_field('procedure_type_header', 34); ?></th>
              <th><?php the_field('average_cost_header', 34); ?></th>
            </tr>

            <?php if( have_rows('procedure_type_table', 34) ): ?>
            <?php while( have_rows('procedure_type_table', 34) ): the_row(); 
              // vars
              $procedureTypeHeader = get_sub_field('procedure_type_cell_header');
              $procedureCostHeader = get_sub_field('procedure_cost_cell_header');
              $procedureTypeName = get_sub_field('procedure_type_name');
              $procedureCost = get_sub_field('procedure_type_cost');
              ?>

              <tr>
                <td>
                  <span class="cell-label"><?php echo $procedureTypeHeader; ?></span>
                  <span class="procedure-type"><?php echo $procedureTypeName; ?></span>
                </td>
                <td>
                  <span class="cell-label"><?php echo $procedureCostHeader; ?></span>
                  <span class="procedure-cost"><?php echo $procedureCost; ?></span>
                </td>
              </tr>
            <?php endwhile; ?>
            <?php endif; ?>
          </table>

          <?php if( have_rows('consider_implants_but_what_about_cost_post_table') ): ?>
              <?php while( have_rows('consider_implants_but_what_about_cost_post_table') ): the_row();
                  ?>
                <?php the_sub_field('consider_implants_post_table_1st_paragraph'); ?>
                <h1 class="sect-hdr-txt primary-blue-txt"><?php the_sub_field('consider_implants_post_2nd_section_header_text'); ?></h1>
                <?php the_sub_field('consider_implants_post_table_2nd_copy_section'); ?>

              <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <section id="home-lifetime-implants" class="primary-blue-bg">
    <div class="section-container mobile-side-padding laptop-side-padding">
      <?php if( have_rows('at_eon_clinics') ): ?>
          <?php while( have_rows('at_eon_clinics') ): the_row();
              ?>
            
            <header class="center-elem tablet-max-540">
              <p class="sect-hdr-txt"><?php the_sub_field('at_eon_clinics_header'); ?></p>
            </header>

            <div class="implant-benefits three-col-icon-desc">
              <?php if( have_rows('at_eon_clinics_icons_and_copy') ): ?>
              <?php while( have_rows('at_eon_clinics_icons_and_copy') ): the_row(); 
                // vars
                $iconImg = wp_get_attachment_image_src(get_sub_field('at_eon_clinics_icon'), 'full');
                $iconTitle = get_sub_field('at_eon_clinics_icon_header');
                $iconCopy = get_sub_field('at_eon_clinics_icon_description');
                ?>


              <div class="benefit">
                <?php
                $svg5 = wp_remote_get($iconImg[0])['body'];
                $dom = new DOMDocument();
                $dom->loadHTML($svg5);
                foreach($dom->getElementsByTagName('svg') as $element) {
                    $element->setAttribute('class','icon');  
                }
                $dom->saveHTML();
                $svg5 = $dom->saveHTML();
                ?>
                <?php echo $svg5; ?>
                <h2 class="sect-hdr2-txt"><?php echo $iconTitle; ?></h2>
                <p><?php echo $iconCopy; ?></p>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>

            <?php 
              $atEonClincCtaBtn = get_sub_field('at_eon_clinics_button_cta');
              if( $atEonClincCtaBtn ): 
                  $atEonClincCtaBtn_url = $atEonClincCtaBtn['url'];
                  $atEonClincCtaBtn_title = $atEonClincCtaBtn['title'];
                  $atEonClincCtaBtn_target = $atEonClincCtaBtn['target'] ? $atEonClincCtaBtn['target'] : '_self';
                  ?>
                  <a rel="nofollow" class="btn primary center-elem" href="<?php echo esc_url( $atEonClincCtaBtn_url ); ?>"><?php echo esc_html( $atEonClincCtaBtn_title ); ?></a>
              <?php endif; ?>

          <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </section>

  <section id="home-personal-treatments" class="light-tan-bg">
    <div class="section-container laptop-side-padding">
      <?php if( have_rows('organic_manage_costs') ): ?>
        <?php while( have_rows('organic_manage_costs') ): the_row();
        
          //vars
          $manageSectionImg = wp_get_attachment_image_src(get_sub_field('manage_costs_section_image'), 'full');
            ?>

          <div class="treatment-visuals">
            <img src="<?php echo $manageSectionImg[0] ?>" alt="" />
          </div>
          <div class="treatment-cost mobile-side-padding tablet-side-padding">
              <h1 class="sect-hdr-txt primary-blue-txt"><?php the_sub_field('manage_costs_section_header_text'); ?></h1>
              <?php the_sub_field('manage_costs_section_paragraphs'); ?>
              <?php 
              $mangeCostLink = get_sub_field('manage_cost_section_final_cta_text_link');
              if( $mangeCostLink ): 
                  $mangeCostLink_url = $mangeCostLink['url'];
                  $mangeCostLink_title = $mangeCostLink['title'];
                  $mangeCostLink_target = $mangeCostLink['target'] ? $mangeCostLink['target'] : '_self';
                  ?>
                  <a rel="nofollow" class="txt-cta" href="<?php echo esc_url( $mangeCostLink_url ); ?>"><?php echo esc_html( $mangeCostLink_title ); ?></a>
              <?php endif; ?>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>


      
    </div>
  </section>

  <section id="faqs-overview">
    <div class="section-container mobile-side-padding tablet-side-padding laptop-side-padding">
      <h1 class="sect-hdr-txt primary-blue-txt max-930 center-elem"><?php the_field('organic_faq_section_header_text'); ?></h1>

      <ul class="accordion faqs">
      <?php if( have_rows('faqs') ): ?>
        <?php while( have_rows('faqs') ): the_row(); 

          $faqQuestion = get_sub_field('faq_question');
          $faqAnswer = get_sub_field('faq_answer');
          ?>

        <li>
          <a class="toggle"><?php echo $faqQuestion; ?><span class="icon-chevron"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 16"><g id="Path_41" data-name="Path 41"><path d="M15.5,15.5.9,2.56A1.12,1.12,0,0,1,.9.85a1.5,1.5,0,0,1,1.93,0L15.5,12.08,28.17.85a1.5,1.5,0,0,1,1.93,0,1.12,1.12,0,0,1,0,1.71Z"/></g></svg></span></a>
          <div class="inner"><?php echo $faqAnswer; ?></div>
        </li>

        <?php endwhile; ?>
        <?php endif; ?>
        
      </ul>
    </div>
  </section>

  <?php if( have_rows('organic_pre_footer_cta') ): ?>
      <?php while( have_rows('organic_pre_footer_cta') ): the_row();
          ?>
        <section class="primary-blue-bg pre-footer-cta">
          <div class="section-container mobile-side-padding tablet-side-padding">
            <div class="pre-footer-content">
              <h1 class="sect-hdr-txt"><?php the_sub_field('pre_footer_cta_text'); ?></h1>
              <?php 
                $preFooterCtaBtn = get_sub_field('pre_footer_cta_button');
                if( $mangeCostLink ): 
                    $preFooterCtaBtn_url = $preFooterCtaBtn['url'];
                    $preFooterCtaBtn_title = $preFooterCtaBtn['title'];
                    $preFooterCtaBtn_target = $preFooterCtaBtn['target'] ? $preFooterCtaBtn['target'] : '_self';
                    ?>
                    <a rel="nofollow" class="btn primary" href="<?php echo esc_url( $preFooterCtaBtn_url ); ?>"><?php echo esc_html( $preFooterCtaBtn_title ); ?></a>
                <?php endif; ?>
            </div>
          </div>
        </section>
    <?php endwhile; ?>
  <?php endif; ?>
  

<?php
get_footer();
?>