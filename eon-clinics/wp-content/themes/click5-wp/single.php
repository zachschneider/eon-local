<?php
//Your access to and use of BrightEdge Link Equity Manager is governed by the 
//Infrastructure Product Terms located at: www.brightedge.com/infrastructure-product-terms. 
//Customer acknowledges and agrees it has read, understands and agrees to be bound by the 
//Infrastructure Product Terms.

//IXF: save the be_ixf_client.php file to your server, then use "require" to include it in your template. 
require 'be_ixf_client.php';
use BrightEdge\BEIXFClient;

//IXF: the following array and constructor must be placed before any HTML is written to the page.
$be_ixf_config = array(
    BEIXFClient::$CAPSULE_MODE_CONFIG => BEIXFClient::$REMOTE_PROD_CAPSULE_MODE,
    BEIXFClient::$ACCOUNT_ID_CONFIG => "f00000000170944",   
	
	//BEIXFClient::$API_ENDPOINT_CONFIG => "https://ixfd-api.bc0a.com",
	//BEIXFClient::$CANONICAL_HOST_CONFIG => "www.domain.com",
	//BEIXFClient::$CANONICAL_PROTOCOL_CONFIG  => "https",
    
	// IXF: By default, all URL parameters are ignored. If you have URL parameters that add value to
	// page content.  Add them to this config value, separated by the pipe character (|).
    BEIXFClient::$WHITELIST_PARAMETER_LIST_CONFIG => "ixf",

);
$be_ixf = new BEIXFClient($be_ixf_config);
?>
<?php

                                //IXF: place getHeadOpen just inside of the HTML head, used for to append SEO-related header elements.

                                print $be_ixf->getHeadOpen();

                ?>
<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

get_header(); ?>

<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item">
                <h1<?php echo update_h1(); ?>><?php the_title(); ?></h1>
            </div>
        </div>
    </div> 
</div>    

<div class="section" id="subpage-content">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-9" id="content-sub">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
                <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                    
                    <div class="entry">
                    

                        <?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>

                        <div class="navigation"><p><?php posts_nav_link(); ?></p></div>

                        <?php the_tags( '<p>Tags: ', ', ', '</p>'); ?>
                    </div>

<!--                     <p class="more">
                    <a href="#"  onclick="goBack()" class="b-read-more">Back</a>
                    </p> -->
                </div>
				
				<!--IXF: The following <div> block needs to be placed in the location where the link equity block will be displayed-->
					<!--IXF: For your website, the location is above/below... -->
					<div class="be-ix-link-block">
					<?php
						print $be_ixf->getBodyString("body_1"); 
						print $be_ixf->close(); 
					?>
					</div>
				<!--IXF: end--> 
<script>
function goBack() {
  window.history.back();
}
</script>
            <?php endwhile; else: ?>

                <p>Sorry, no posts matched your criteria.</p>

            <?php endif; ?>
            <?php edit_post_link('Edit this post', '<p class="cboth"><br />', '</p>'); ?>
                
            </div>

            <div class="col-lg-3" id="sidebar-area">
                <?php get_sidebar(); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>
