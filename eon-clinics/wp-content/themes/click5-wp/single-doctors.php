<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

get_header(); ?>

<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item">
                <h1<?php echo update_h1(); ?>>EON Clinics Doctors</h1>
            </div>
        </div>
    </div> 
</div>    

<div class="section" id="subpage-content">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-9" id="content-sub">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
                <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                    
                    <div class="entry">
                    
                        <h3><?php the_title(); ?></h3>

                        <?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>

                        <div class="navigation"><p><?php posts_nav_link(); ?></p></div>

                        <?php the_tags( '<p>Tags: ', ', ', '</p>'); ?>
                    </div>
                </div>

            <?php endwhile; else: ?>

                <p>Sorry, no posts matched your criteria.</p>

            <?php endif; ?>
            <?php edit_post_link('Edit this post', '<p class="cboth"><br />', '</p>'); ?>
                
            </div>

            <div class="col-lg-3" id="sidebar-area">
                <?php get_sidebar(); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>
