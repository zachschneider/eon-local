<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */
?>

<?php get_template_part( 'parts/part', 'cta' ); ?>

<div class="section" id="contact">
    <div class="container">
        <div class="row">
            <div class="item col-12 col-sm-6 col-lg d-flex align-items-start flex-column">
                <h4>Hoffman Estates</h4>
                <p class="mb-auto">1585 N. Barrington Rd #301<br>Hoffman Estates, IL 60169<br>(Doctor Building #2)</p>
                <p class="phone"><a href="tel:8443657645">844-365-7645</a></p>
          		<p class="link"><a href="/dental-implants-hoffman-estates" title="Dental Implants Center in Hoffman Estates">Get Directions </a></p>
            </div>
            <div class="item col-12 col-sm-6 col-lg d-flex align-items-start flex-column">
                <h4>Skokie</h4>
                <p class="mb-auto">5215 Old Orchard Rd #200<br>Skokie, IL 60077</p>
                <p class="phone"><a href="tel:8443657645">844-365-7645</a></p>
                <p class="link"><a href="/dental-implants-skokie" title="Dental Implants Center in Skokie">Get Directions </a></p>
            </div>
            <div class="item col-12 col-sm-6 col-lg d-flex align-items-start flex-column">
                <h4>Oakbrook Terrace</h4>
                <p class="mb-auto">17W110 22nd St, Ste 150<br>Oakbrook Terrace, IL 60181</p>
                <p class="phone"><a href="tel:8443657645">844-365-7645</a></p>
                <p class="link"><a href="/dental-implants-downers-grove-westmont" title="Dental Implants Center in Downers Grove & Westmont">Get Directions </a></p>
            </div>
            <div class="item col-12 col-sm-6 col-lg d-flex align-items-start flex-column">
                <h4>Munster</h4>
                <p class="mb-auto">10012 Calumet Ave<br>Munster, IN 46321</p>
                <p class="phone"><a href="tel:8443657645">844-365-7645</a></p>
                <p class="link"><a href="/dental-implants-munster" title="Dental Implants Center in Munster">Get Directions </a></p>
            </div>
            <div class="item col-12 col-sm-6 col-lg d-flex align-items-start flex-column">
                <h4>Waukesha</h4>
                <p class="mb-auto">20700 Swenson Dr<br>Waukesha, WI 53186</p>
                <p class="phone"><a href="tel:8443657645">844-365-7645</a></p>
                <p class="link"><a href="/dental-implants-waukesha" title="Dental Implants Center in Waukesha">Get Directions </a></p>
            </div>
        </div>
    </div>
</div>

<div class="section" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <hr class="big" />
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <img class="logo-foot" src="<?php bloginfo('template_url'); ?>/images/logo.svg" />
            </div>
            <div class="col-md-6 d-flex justify-content-md-end align-items-center">
                <div class="social">
                    <a href="https://www.facebook.com/eonclinics" target="_blanc" title="Follow Us on Facebook"><i class="fa fa-facebook"></i></a>
                    
                    <a href="https://www.youtube.com/channel/UCFBF08WL46BSiDbniUbSeFA" target="_blanc" title="View Us on Youtube"><i class="fa fa-youtube-play"></i></a>
                   
                </div>
            </div>
            <div class="col-md-12">
                <hr />
            </div>
            <div class="col-md-6 copy d-flex align-items-center">
                &copy; <?php echo date('Y'); ?> EON Clinics. All rights reserved. Site Designed by <a href="https://www.emsc.com/" rel="nofollow" title="Contact Us">EMSC</a>
            </div>
            <div class="col-md-6 copy d-flex justify-content-md-end align-items-center">
                <?php wp_nav_menu(array('menu' => 'Footer Navigation')); ?>
            </div>
        </div>
    </div>
</div>

<div id="contact-sticky">
    <a href="/scheduler">
        <!--<img src="<?php bloginfo('template_url'); ?>/images/icon-sticky.png" />-->
		Schedule a <strong>Free Consultation</strong>
    </a>
</div>

<div id="contact-sticky2">
    <a href="tel:8443657645">
       <i class="fas fa-phone fa-flip-horizontal"></i> Call <strong>Now</strong>
    </a>
</div>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7Z9KJW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->

<?php wp_footer(); ?>

</body>
</html>
