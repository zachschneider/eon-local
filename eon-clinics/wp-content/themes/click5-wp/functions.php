<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

// register sidebar for widgets
if ( function_exists('register_sidebar') ) {
	register_sidebar( array(
		'name' => 'Sidebar Widget Area',
		'id' => 'sidebar-widget-area',
		'description' => 'Sidebar widget area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
	) );
}

// enable shortcodes in widgets
add_filter('widget_title', 'do_shortcode');
add_filter('widget_text', 'do_shortcode');
add_filter('the_title', 'do_shortcode');


// remove WordPress version from generator tag
function generic_generator_tag($generator) {
	return str_replace(' '.get_bloginfo('version'), '', $generator);
}
add_filter('the_generator', 'generic_generator_tag');

// enable WordPress menus

function register_my_menus() {
  register_nav_menus(
    array(
      'main-navigation' => __( 'Main Navigation' ),
      'footer-navigation' => __( 'Footer Navigation' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

// custom walker for mobile nav

class Walker_Mobile_Nav_Menu extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown\">\n";
	}
}

// enabling support for post thumbnails 
add_theme_support('post-thumbnails');

add_theme_support('automatic-feed-links');

// CUSTOM POST TYPE

function create_posttype() {
	register_post_type( 'doctors',
    array(
      'labels' => array(
        'name' => __( 'Doctors' ),
        'singular_name' => __( 'Doctor' )
      ),
      'public' => true,
      'has_archive' => false,
      'rewrite' => array('slug' => 'doctors','with_front' => false),
      'taxonomies' => array( 'category' ),
      'menu_position' => 5,
      'supports' => array(
          'title',
          'thumbnail',
					'editor',
					'excerpt' )
    )
  );
	}
add_action( 'init', 'create_posttype' );

flush_rewrite_rules( false );




// Enqueue scripts and styles.
function click5_scripts() {

		$the_theme = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

    wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(), $theme_version );
    wp_enqueue_style( 'owl-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css', array(), $theme_version );
    wp_enqueue_style( 'owl-carousel-theme', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css', array(), $theme_version );
    wp_enqueue_style( 'slicknav', get_template_directory_uri() . '/css/slicknav.css', array(), $theme_version );
    wp_enqueue_style( 'bootstrap-grid', get_template_directory_uri() . '/css/bootstrap-grid.css', array(), $theme_version );
    wp_enqueue_style( 'bootstrap-grid', get_template_directory_uri() . '/css/fonts.css', array(), $theme_version );
    wp_enqueue_style( 'modal-video', get_template_directory_uri() . '/css/modal-video.min.css', array(), $theme_version );
		wp_enqueue_style( 'click5-style', get_stylesheet_uri(), array(), $theme_version );
    wp_enqueue_style( 'click5-responsive', get_template_directory_uri() . '/css/responsive.css', array(), $theme_version );

    wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), $theme_version );
    wp_enqueue_script( 'jquery-validate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js', array(), $theme_version );
       wp_enqueue_script( 'owl', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js', array(), $theme_version );
		wp_enqueue_script( 'masked', get_template_directory_uri() . '/js/jquery.maskedinput.min.js', array(), $theme_version );
    wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.js', array(), $theme_version );
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', array(), $theme_version );
    wp_enqueue_script( 'headhesive', get_template_directory_uri() . '/js/headhesive.min.js', array(), $theme_version );
    wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/jquery.slicknav.js', array(), $theme_version );
    wp_enqueue_script( 'modal-video-js', get_template_directory_uri() . '/js/jquery-modal-video.min.js', array(), $theme_version );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/global.js', array(), $theme_version );
	 wp_enqueue_script( 'main', get_template_directory_uri() . '/js/action.js', array(), $theme_version );
    
}
add_action( 'wp_enqueue_scripts', 'click5_scripts' );


// page pagination
function click5_pagenavi($before = '', $after = '', $prelabel = '', $nxtlabel = '', $pages_to_show = 5, $always_show = false) {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	
	
	
	if(empty($prelabel)) {
		$prelabel  = '<strong>&laquo;</strong>';
	}
	if(empty($nxtlabel)) {
		$nxtlabel = '<strong>&raquo;</strong>';
	}
	$half_pages_to_show = round($pages_to_show/2);
	if (!is_single()) {
		if(!is_category()) {
			preg_match('#FROM\s(.*)\sORDER BY#siU', $request, $matches);		
		} else {
			preg_match('#FROM\s(.*)\sGROUP BY#siU', $request, $matches);		
		}
		$fromwhere = $matches[1];
		$numposts = $wp_query->found_posts;
		$max_page = $wp_query->max_num_pages;
		
		if(empty($paged) || $paged == 0) {
			$paged = 1;
		}
		if($max_page > 1 || $always_show) {
			echo "$before <div class='nav'>";
			if ($paged >= ($pages_to_show-1)) {
				echo '<a href="'.get_pagenum_link().'">&laquo; First</a>';
			}
			previous_posts_link($prelabel);
			for($i = $paged - $half_pages_to_show; $i  <= $paged + $half_pages_to_show; $i++) {
				if ($i >= 1 && $i <= $max_page) {
					if($i == $paged) {
						echo "<strong class='on'>$i</strong>";
					} else {
						echo ' <a href="'.get_pagenum_link($i).'">'.$i.'</a> ';
					}
				}
			}
			next_posts_link($nxtlabel, $max_page);
			if (($paged+$half_pages_to_show) < ($max_page)) {
				echo '<a href="'.get_pagenum_link($max_page).'">Last &raquo;</a>';
			}
			echo "</div> $after";
		}
	}
}

function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);

      if (count($excerpt) >= $limit) {
          array_pop($excerpt);
          $excerpt = implode(" ", $excerpt) . '...';
      } else {
          $excerpt = implode(" ", $excerpt);
      }

      $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

      return $excerpt;
}

function check_background() {

	if ( has_post_thumbnail() ){
			//$featured_img_url = get_the_post_thumbnail_url(get_the_ID());
			 echo " style=\"background-image:url('" . get_the_post_thumbnail_url(get_the_ID()) . "');\"";
	} else {
			//$featured_img_url = bloginfo('template_url') . '/images/subpage-bg.jpg';
			 echo " style=\"background:" . get_field("hero_background_color") . ";" . " padding-bottom:30px;\"";
	}
	return $featured_img_url;
}



function update_h1() {
	
			if (get_field("hero_headline_width")) {
				$heroH1width = get_field("hero_headline_width");
				$heroH1wcolor = get_field("hero_headline_color");
			} else {
				$heroH1width = 55;
			}
			
			echo " style=\"width:" . get_field("hero_headline_width") . "%; color:" . get_field("hero_headline_color") . ";\"";


}



add_action( 'wp_footer', 'redirect_cf7' );
 
function redirect_cf7() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
   if ( '408' == event.detail.contactFormId ) { // Sends submissions on form 408 to the first thank you page
    location = '/dental-implants-cost/teeth-replacement-cost-ebook-thank-you';
    } else if ( '116' == event.detail.contactFormId ) { // Sends submissions on form 116 to the second thank you page
        location = '/teeth-replacement-ebook-thank-you';
    } else if ( '515' == event.detail.contactFormId ) { // Sends submissions on form 515 to the second thank you page
        location = '/teeth-replacement-ebook-thank-you';
    } 
    else if ( '123' == event.detail.contactFormId || '846' == event.detail.contactFormId) { // Sends submissions on form 515 to the second thank you page
        location = '/scheduler-location-archived-2';
    }
    else if ( '561' == event.detail.contactFormId ) { // Sends submissions on form 515 to the second thank you page
        location = '/question-thank-you';
    }    
	else {
		
    }
}, false );
</script>
<?php
}

?>