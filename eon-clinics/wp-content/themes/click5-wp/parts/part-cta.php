<?php
/**
 * CTA Section
 *
 * @package iconbrand
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div class="section" id="cta">
    <div class="container">
        <div class="row">
            <div class="col-md-12 d-flex flex-wrap justify-content-center align-items-center">
                <h3>See how dental implants can improve your life!</h3>
                <a href="https://www.eonclinics.com/scheduler/" title="Schedule Your Free Consultation"><button>Schedule a Free Consultation </button></a>
            </div>
        </div>
    </div>
</div>