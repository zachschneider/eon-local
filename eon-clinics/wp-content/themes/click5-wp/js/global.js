jQuery(function($){
    $(document).ready(function () {

        $("input[type=tel]").mask("(999) 999-9999");

        $('#menu-mobile').slicknav();

        $(".js-video-button").modalVideo({
            youtube:{
                controls:0,
                nocookie: true,
                rel: 0,
                autoplay: 1
            }
        });

        var headerheight = $('header').height();

        $(document).on('scroll', function () {

            if ($(document).scrollTop() > headerheight) {

                $('header').addClass('sticky');

            } else {

                $('header').removeClass('sticky');

            }
			if($(window).width() >= 850){
            if ($(document).scrollTop() > 800) {
                $('#contact-sticky').fadeIn();
            } else {
                $('#contact-sticky').fadeOut();

            }
            }
            
            if($(window).width() <= 850){
				if ($(document).scrollTop() > 200) {
                $('#contact-sticky2',).fadeIn();
                $('#contact-sticky').fadeIn();
                
            } else {
                $('#contact-sticky2').fadeOut();
                 $('#contact-sticky').fadeOut();

            }
			
		}
            

        });

		
		
        $("#testimonial-slider").owlCarousel({
            items:3,
            itemsDesktop:[1000,3],
            itemsDesktopSmall:[979,1],
            itemsTablet:[768,1],
            pagination: true,
            autoPlay:true
        });
		$('.accordion-toggle' ).click(function(){

			jQuery( this ).parent( '.group' ).toggleClass( 'active' );
			jQuery( this ).parent( '.group' ).siblings().removeClass( 'active' );
			
			jQuery(this).next().slideToggle( 'fast' );

			jQuery( '.accordion-content' ).not( jQuery(this).next()).slideUp( 'fast' );

		});
		
		$('.accordion-toggle').click( function(){
    if ( $(this).hasClass('current') ) {
        $(this).removeClass('current');
    } else {
        $('.accordion-toggle.current').removeClass('current');
        $(this).addClass('current');    
    }
});

    });
	
});
