<?php

/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

session_start(); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?></title>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,500,700,900&amp;subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet">
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.png" type="image/x-icon" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/images/apple-touch-icon-iphone-retina.png" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/images/apple-touch-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/images/apple-touch-icon-iphone.png" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />



<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M7Z9KJW');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-57874228-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-57874228-1');
</script>

<?php wp_head(); ?>
<script type='application/ld+json'>
{
  "@context": "http://www.schema.org",
  "@type":"Dentist",
  "name": "EON Clinics",
  "URL": "https://www.eonclinics.com/",
  "image": "https://www.eonclinics.com/wp-content/themes/click5-wp/images/logo.png",
"priceRange": "$",
  "address":
 [
   { "@type": "PostalAddress", "streetAddress": "1585 North Barrington Rd #301", "addressLocality": "Hoffman Estates", "addressRegion": "IL", "postalCode": "60169", "telephone": "844-365-7645" },
   { "@type": "PostalAddress", "streetAddress": "10012 Calumet Ave", "addressLocality": "Munster", "addressRegion": "IN", "postalCode": "46321", "telephone": "844-365-7645" },
   { "@type": "PostalAddress", "streetAddress": "5215 Old Orchard Rd #200", "addressLocality": "Skokie", "addressRegion": "IL", "postalCode": "60077", "telephone": "844-365-7645" },
   { "@type": "PostalAddress", "streetAddress": "20700 Swenson Dr", "addressLocality": "Waukesha", "addressRegion": "WI", "postalCode": "53186", "telephone": "844-365-7645" },
   { "@type": "PostalAddress", "streetAddress": "6319 Fairview Ave, Suite 103", "addressLocality": "Westmont", "addressRegion": "IL", "postalCode": "60559", "telephone": "844-365-7645" }
 ]
}
</script>
</head>

<body <?php body_class(); ?>> 
      
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 d-flex justify-content-end align-items-center" id="nav">
                <a class="d-flex align-items-center" href="https://www.eonclinics.com" title="Same Day & All On 4 Dental Implants Center">
                    <img class="logo"  data-no-lazy="1" src="<?php bloginfo('template_url'); ?>/images/logo.svg" alt="EON Clinics - Same Day & All On 4 Dental Implants Center" />
                </a>
				 <?php wp_nav_menu(array(
                    'menu' => 'Main Navigation',
                    'container' => false,
                    'menu_class' => 'd-flex align-items-center flex-wrap justify-content-end'
                )); ?>
				 
            </div>
            <div class="col-lg-4 hidden-lg  d-flex justify-content-end align-items-center" style="padding-left: 10px">
             <a href="tel:844-365-7645" style="color: #142b4e;text-decoration:none;">844-365-7645</a>
                <div class="button-call">
                    <a href="/scheduler-new" id="header_schedule" title="Schedule Your Free Consultation"><button class="light-white d-flex align-items-center" id="nav-schedule-cta">Schedule Free Consultation</button></a>
					<a href="tel:8443657645" id="header_phone"><button class="light-white d-flex align-items-center"><img src="<?php bloginfo('template_url'); ?>/images/phone-icon.png" alt="Call Us to Schedule A Free Consultation">844-365-7645</button></a>
                </div>
            </div>
        </div>
    </div>  
</header>
        
<div id="select-nav">   
	<?php
    $defaults = array(
        'theme_location'  => '',
        'menu'            => 'Main Navigation',
        'menu_id'         => 'menu-mobile',
        'walker'          => new Walker_Mobile_Nav_Menu(),
    );
    wp_nav_menu( $defaults );
    ?>
</div>  
