<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */
 /*
	 Template Name: FAQ
*/

get_header(); ?> 

<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item">
                <h1<?php echo update_h1(); ?>><?php the_title(); ?></h1>
            </div>
        </div>
    </div> 
</div>    

<div class="section" id="subpage-content">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-9" id="content-sub">

               <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		        <div class="entry">
		            
		            <h2 class="page-title">Frequently Asked Questions - <span style="font-weight: 700;"><?php the_title(); ?></span></h2>
					<p>Click a question below to reveal its answer.</p>
		            <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

		               
		
		        </div>
		        <?php endwhile; endif; ?>
			        	
			        	
			        	
			        	<?php
			if( have_rows('accordian') ):
		?>
  				<div class="faq">

						<div class="accordion">
     				<?php
    					while( have_rows('accordian') ) : the_row()								
					?>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "<?php the_sub_field('question'); ?>",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "<?php the_sub_field('answer'); ?>"
    }
  }]
  }
</script>
       			<div class="group">

                                <div>
                                 <div >
                                 <section>

									<h4 class="accordion-toggle">
										<span><?php the_sub_field('question'); ?></span>
									</h4>
									<div class="accordion-content">
										<span><span><?php the_sub_field('answer'); ?></span></span>
									</div>

                                 </section>
                                  </div>
                                 </div>

								</div>
    	 			<?php
        			$counter++;
        			if ($counter % 4 === 0):
            		echo '</div></div> <div class="faq"> <div class="accordion">';
        			endif; ?>
					<?php endwhile;?>
				</div>
  				</div>
			<?php endif; ?>
			
			    
        		<p class="more" style="padding-left: 0px;"><a href="/dental-implant-faqs/">Back to FAQS</a></p>
                
            </div>

            <div class="col-lg-3" id="sidebar-area">
                <?php get_sidebar(); ?>
            </div>

        </div>
    </div>
</div>


<?php get_footer(); ?>
