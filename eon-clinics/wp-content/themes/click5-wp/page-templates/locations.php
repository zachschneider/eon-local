<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

/*
Template Name: Locations
*/

get_header(); ?>

<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item">
                <h1<?php echo update_h1(); ?>><?php the_title(); ?></h1>
            </div>
        </div>
    </div> 
</div>    

<div class="section locations-page" id="subpage-content">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-9" id="content-sub">
	            <div class="entry">
	            <h3><?php the_field('sub_header'); ?></h3>
	            </div>
                <div class="row" id="location-block">
					
                    <div class="location-info col-md-6">
						<strong><h3> Our Doctors</h3></strong></br>
                       <div class="doctors-list">
						   
                    <?php if ( have_rows('doctors_list') ) : ?>
                        
                        <?php while( have_rows('doctors_list') ) : the_row(); ?>
                    
                            <div class="doctors-item text-center">
                                <a href="<?php the_sub_field('doctor_page'); ?>">
                                    <img src="<?php the_sub_field('doctor_photo'); ?>" alt="<?php the_sub_field('doctor_name'); ?>">
                                    <h4><?php the_sub_field('doctor_name'); ?></h4>
                                </a>
                            </div>
                    
                        <?php endwhile; ?>
                    
                    <?php endif; ?>

                </div> 
                    </div>
                    <div class="location-info col-md-6">
	                    <strong><h3> Contact Us</h3></strong></br>
						  <div class="row">
							  <div class="col-md-12 d-flex">
								  <i class="fa fa-phone" aria-hidden="true"></i>
                        <p><a href="tel:<?php the_field('location_phone'); ?>"><?php the_field('location_phone'); ?></a></p>
							  </div>
							    <div class="col-md-12 d-flex">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <p><?php the_field('location_address'); ?></p>
							  </div>
						</div>
                    </div>
                </div>

               

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   <div class="entry">
                        <?php the_content(); ?>
                   </div>
                <?php endwhile; endif; ?>               

            </div>

            <div class="col-lg-3" id="sidebar-area">
                <?php get_sidebar(); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>
