<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

/*
Template Name: Schedule Step 2
*/

get_header(); ?>

<div class="hero-sub schedule d-flex align-items-end">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item align-items-center text-center">
                <h1 id="schedule_h1" <?php echo update_h1(); ?>>Schedule Your Free Consultation<br><strong>In 3 Easy Steps</strong></h1>
            </div>
        </div>
		<div class="row">
			<img src="wp-content/uploads/2019/08/ssl-secure-encryption.png" style="width:120px; height: 39px; margin: 0 auto;"/>
		</div>
    </div> 
</div>    

<div class="v2" id="schedule">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="offset-lg-1 col-lg-10 form-wrapper">
                <div class="item">
					<div class="row">
<div class="nav-progress">
<div class="bar complete">
1 About You
<div class="arrow-wrapper">
<div class="arrow-cover">
<div class="arrow"></div>
</div>
</div>
</div>
<div class="bar complete">
2 Location
<div class="arrow-wrapper">
<div class="arrow-cover">
<div class="arrow"></div>
</div>
</div>
</div>
<div class="bar">
3 Date/Time
</div>
</div>
                  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="entry">
                          <?php the_content(); ?>
                    </div>
                  <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php //get_footer(); ?>
