<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

/*
Template Name: Doctors
*/

get_header(); ?>

<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item">
                <h1<?php echo update_h1(); ?>><?php the_title(); ?></h1>
            </div>
        </div>
    </div> 
</div>    

<div class="section" id="subpage-content">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-9" id="content-sub">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   <div class="entry">
                        <?php the_content(); ?>
                   </div>
                <?php endwhile; endif; ?>

                <div class="doctors-list" id="dp">
                    <?php if ( have_rows('doctors_list') ) : ?>
                        
                        <?php while( have_rows('doctors_list') ) : the_row(); ?>
                    
                            <div class="doctors-item text-center">
                                <a href="<?php the_sub_field('doctor_page'); ?>">
                                    <img src="<?php the_sub_field('doctor_photo'); ?>" alt="<?php the_sub_field('doctor_name'); ?>">
                                    <h4><?php the_sub_field('doctor_name'); ?></h4>
                                </a>
                            </div>
                    
                        <?php endwhile; ?>
                    
                    <?php endif; ?>

                </div>                

            </div>

            <div class="col-lg-3" id="sidebar-area">
                <?php get_sidebar(); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>
