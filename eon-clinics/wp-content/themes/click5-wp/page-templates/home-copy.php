<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

/*
Template Name: Home Copy
*/

get_header(); ?>

<div class="section d-flex align-items-center" id="hero">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>A <strong>smile</strong> is a sign<br>of <strong>hope</strong></p>
            </div>
        </div>
    </div>
</div>

<div class="section" id="main-banners">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banners-container">
                    <div class="row no-gutters">
                        <div class="item white col-lg-6">
                            <a href="tel:8443657645"  style="padding: 45px 15px" class="main-link d-flex align-items-center justify-content-center">
                                <p style="font-size: 27px;" id="homebox">Call Us Today! <strong>844-365-7645</strong> </p>
                            </a>
                        </div>
                        <div class="item green col-lg-6">
                            <a href="/scheduler"  style="padding: 45px 15px" class="main-link d-flex align-items-center justify-content-center">
                                <p style="font-size: 27px; color: #fff;" id="homebox">Schedule a <strong>Free Consultation</strong></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section" id="about">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-6 item">
                <h1><strong>EON Clinics</strong> - Same Day &amp; All-on-4 Dental Implants Center in Chicago and Milwaukee </h1>
            </div>
            <div class="col-lg-6 item text-right">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                <?php endwhile; endif; ?>
                <p class="more"><a href="/same-day-implants">Learn more</a></p>
            </div>
        </div>
    </div>
</div>

<div id="services">
    <div class="section">
        <div class="container">

            <div class="row no-gutters">
                <div class="photo col-lg-6" style="background-image: url('/wp-content/uploads/2019/06/service3-1-new.jpg')">
                </div>
                <div class="text col-lg-6">
                    <h2>Types of Dental Implants</h2>
                    <p>Find out more about the different procedures we offer, including our full jaw teeth restorations that look just like real teeth. With dental implants, you'll experience greater comfort than with dentures. And you'll be able to eat the foods you like!</p>
                    <p class="more"><a href="/types-of-implants">Learn more</a></p>
                </div>
            </div>

            <div class="row no-gutters">
                <div class="photo col-lg-6 order-1 order-lg-2" style="background-image: url('/wp-content/uploads/2019/06/service2.jpg')">
                </div>
                <div class="text col-lg-6 order-2 order-lg-1">
                    <h2>Benefits of Dental Implants</h2>
                    <p>Want to know more about why you should choose dental implants? Not only do they look and work like real teeth, they offer several key health benefits. Conquer bone loss and infection with a solution that's designed to last a lifetime.</p>
                    <p class="more"><a href="/dental-implant-benefits">Learn more</a></p>
                </div>
            </div>

            <div class="row no-gutters">
                <div class="photo col-lg-6" style="background-image: url('/wp-content/uploads/2019/05/dental-implants-seniors.jpg')">
                </div>
                <div class="text col-lg-6">
                    <h2>Cost of Dental Implants</h2>
                    <p>Discover why dental implants are the worthwhile solution that save you money in the long run compared to other teeth replacement alternatives. Learn about our financing options for your procedure and invest in a priceless smile.</p>
                    <p class="more"><a href="/dental-implants-cost">Learn more</a></p>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="section" id="why">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Why You Can Trust<small>Your Smile To EON Clinics</small></h2>
            </div>
        </div>
    </div>
</div>

<div class="section" id="why-items">
    <div class="container">
        <div class="row no-gutters">
            <div class="item col-lg-4 d-flex flex-column justify-content-between">
                <div>
                    <?php /*emsc.mm1 <img src="<?php bloginfo('template_url'); ?>/images/icon-owned.png" alt=""> */ ?>
                    <h3>Family-Owned<br>Dental Implant Center</h3>
                    <p>With the most Chicagoland locations, EON marries the benefits of a larger clinic with the personal attention and care of a family-owned and operated practice.</p>
                </div>
                <p class="more"><a href="/eon-clinics-right-choice">Learn More</a></p>
            </div>
            <div class="item col-lg-4 d-flex flex-column justify-content-between">
                <div>
                    <?php /*emsc.mm1 <img src="<?php bloginfo('template_url'); ?>/images/icon-day.png" alt=""> */ ?>
                    <h3>Single Day<br>in One Location</h3>
                    <p>Each EON location houses your entire medical, dental and lab team, which makes it convenient for you to get your new smile in just one day!</p>
                </div>
                <p class="more"><a href="/eon-clinics-right-choice">Learn More</a></p>
            </div>
            <div class="item col-lg-4 d-flex flex-column justify-content-between">
                <div>
                    <?php /*emsc.mm1 <img src="<?php bloginfo('template_url'); ?>/images/icon-quality.png" alt=""> */ ?>
                    <h3>Quality Care<br>& Materials</h3>
                    <p>Our medical staff is second to none and we use only the highest quality materials to fabricate your implants.</p>
                </div>
                <p class="more"><a href="/eon-clinics-right-choice">Learn More</a></p>
            </div>
        </div>
    </div>
</div>

<div class="section" id="information">
    <div class="container">
        <div class="row">
            <div class="text col-lg-4">
                <h3 class="section-title">Dental Implant<br>Information</h3>
                <ul>
                    <li><a href='/dental-implant-benefits/' title="Benefits of Dental Implants">Why Implants?</a></li>
                    <li><a href='/types-of-implants/' title="Types of Dental Implants">Types of Implants</a></li>
                    <li><a href='/dental-implants-candidate/' title="Qualifies for Dental Implants">Are You a Candidate?</a></li>
                    <li><a href='/dental-implants-cost/' title="Dental Implants Cost Comparison with Other Options">Cost Comparison</a></li>
                    <li><a href='/dental-implants-financing-insurance/' title="Dental Implants Financing & Insurance">Paying for Dental Implants</a></li>
                    <li><a href='/dental-implant-faqs/' title="FAQs About Dental Implants">FAQ</a></li>
                </ul>
            </div>
            <div class="film col-lg-7 offset-lg-1 d-flex align-items-center js-video-button" data-video-id="NfrhYo3WjSA">
                <span><img src="<?php bloginfo('template_url'); ?>/images/film-cover.jpg" alt=""></span>
            </div>
        </div>
    </div>
</div>

<div class="section" id="testimonials">
    <div class="container">
        <div class="row">

            <div class="col-md-12 text-center">
                <h3 class="section-title">Our Patients Say It Best</h3>
            </div>

            <div id="testimonial-slider" class="owl-carousel">

                <?php if ( have_rows('testimonials') ) : ?>

                    <?php while( have_rows('testimonials') ) : the_row(); ?>

                        <div class="testimonial">
                            <div class="description text-center d-flex flex-column justify-content-between">
                                <div>
                                    <div class="client-photo">
                                        <img src="<?php the_sub_field('client_photo'); ?>" alt="">
                                    </div>
                                    <p><?php the_sub_field('testi_text'); ?></p>
                                </div>
                                <button class="js-video-button" data-video-id="<?php the_sub_field('youtube_id'); ?>"><i class="fa fa-youtube-play"></i>Watch Video</button>
                            </div>
                        </div>

                    <?php endwhile; ?>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
