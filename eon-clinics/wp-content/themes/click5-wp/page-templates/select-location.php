<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */
/* Template Name: Full Width */
get_header(); ?> 

<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?> >
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item">
                <h1<?php echo update_h1(); ?>><?php the_title(); ?></h1>
            </div>
        </div>
    </div> 
</div>    

<div class="section" id="subpage-content">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-12" id="content-sub">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   <div class="entry">
                        <?php the_content(); ?>
                   </div>
                <?php endwhile; endif; ?>
                
            </div>

       

        </div>
    </div>
</div>


<?php get_footer(); ?>
