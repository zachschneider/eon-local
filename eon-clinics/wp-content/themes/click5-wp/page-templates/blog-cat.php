<?php
   /**
    * @package WordPress
    * @subpackage click5_WP_Theme
    */
   /* Template Name: Blog Categories */
   
   get_header(); ?>
<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?> >
   <div class="container">
      <div class="row">
         <div class="col-lg-12 item">
            <h1<?php echo update_h1(); ?>><?php the_title(); ?></h1>
         </div>
      </div>
   </div>
</div>
<div class="section" id="subpage-content">
   <div class="container">
      <div class="row no-gutters">
         <div class="col-lg-9" id="content-sub">
            <?php
               $paged = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
               if (is_page('253')) {
               $args = array(
                      'post_type' => 'post',
                      'post_status' => 'publish',
                      'category_name' => 'dental-health',
                      'posts_per_page' => 5,
                      'paged' => $paged,
                  );
               
               } elseif ( is_page( '181' ))  {  
               $args = array(
                      'post_type' => 'post',
                      'post_status' => 'publish',
                      'category_name' => 'press',
                      'posts_per_page' => 5,
                      'paged' => $paged,
                  );
               
               
               }
               elseif ( is_page( '255' ))  {  
               $args = array(
                      'post_type' => 'post',
                      'post_status' => 'publish',
                      'category_name' => 'implant-information',
                      'posts_per_page' => 5,
                      'paged' => $paged,
                  );
               
               
               }
               elseif ( is_page( '257' ))  {  
               $args = array(
                      'post_type' => 'post',
                      'post_status' => 'publish',
                      'category_name' => 'pricing-and-insurance',
                      'posts_per_page' => 5,
                      'paged' => $paged,
                  );
               
               
               }    
               
               else { 
               
               } 
      
                  $paged = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
                
                  $arr_posts = new WP_Query( $args );
               
                  if ( $arr_posts->have_posts() ) :
               
                      while ( $arr_posts->have_posts() ) :
                          $arr_posts->the_post();
                          ?>
            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
               <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
               <p class="postpublishdate"><small>Posted on <?php the_time('F jS, Y') ?> by <?php the_author() ?></small></p>
               <div class="entry">
                  <?php the_excerpt('Read the rest of this entry &raquo;'); ?>
               </div>
               <p class="more" style="padding-left: 0;"><a href="<?php the_permalink() ?>" class="b-read-more">read more</a></p>
            </div>
            <?php endwhile; ?>
            <div class="pagination">
               <?php wp_pagenavi(
                  array(
                      'query' => $arr_posts,
                  )
                  ); ?> 
            </div>
            <?php else : ?>
            <h1>Not Found</h1>
            <p>Sorry, but you are looking for something that isn't here. Perhaps sitemap will help.</p>
            <?php echo ddsg_create_sitemap(); ?>
            <?php endif; ?>
         </div>
         <div class="col-lg-3" id="sidebar-area">
            <?php get_sidebar(); ?>
         </div>
      </div>
   </div>
</div>
<?php get_footer(); ?>