<?php
   /**
    * @package WordPress
    * @subpackage click5_WP_Theme
    */
   
   /*
   Template Name: Less Text 
   */
   
   get_header(); ?>
<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?> >
   <div class="container">
      <div class="row">
         <div class="col-lg-12 item">
            <h1<?php echo update_h1(); ?>><?php the_title(); ?></h1>
         </div>
      </div>
   </div>
</div>
<div class="section" id="information">
   <div class="container">
      <div class="row">
         <div class="text col-lg-6 align-self-center">
            <h3 class="section-title" style="font-weight: 600;">See firsthand what dental<br>implants can do for your life</h3>
            <div class="less-text">
               <p>Patti, a registered nurse, knew she did not want to go the denture route after caring for many patients that struggled with their dentures. Discover why Patti chose EON Clinics for her dental implants.</p>
            </div>
         </div>
         <div class="film col-lg-6  d-flex align-items-center js-video-button" data-video-id="NfrhYo3WjSA">
            <span><img src="<?php bloginfo('template_url'); ?>/images/film-cover.jpg" alt=""></span>
         </div>
      </div>
      <div id="rates">
         <div class="row justify-content-md-center">
            <div class="col-md-2 text-center">
               <h1>#1</h1>
               <strong>Way to Recreate Teeth</strong>
            </div>
            <div class="col-md-2 text-center">
               <h1>98%</h1>
               <strong>Success<br>Rate</strong>
            </div>
            <div class="col-md-2 text-center">
               <h1>20</h1>
               <strong>Years of<br>Durability</strong>
            </div>
            <div class="col-md-2 text-center">
               <h1>0</h1>
               <strong>Zero maintenance<br>over time</strong>
            </div>
            <div class="col-md-2 text-center">
               <h1>100%</h1>
               <strong>Of the Power of Natural Teeth</strong>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="services" >
<div class="section" style="background-color: #F3F8F8; padding-top: 60px; padding-bottom: 60px;">
   <div class="container">
      <div class="row no-gutters">
         <div class="photo col-lg-6">
		 	<img src="/wp-content/uploads/2019/05/Screen-Shot-2019-04-23-at-3.58.48-PM.png" width="100%" heigh="auto">
			<div style="background: #5FB987; text-align: center; padding: 25px">
				<a href="#" style="text-decoration: none; color: white; font-family: Montserrat; font-style: normal; font-weight: bold; font-size: 22px;">Meet The Doctors</a><img style="position: absolute; margin-top: -65px; margin-left: 50px;" src="/wp-content/uploads/2019/06/Oval-1.png">
			 </div> 
         </div>
         <div class="text col-lg-6">
             <h4 class="section-title" style="font-weight: 600;font-size: 30px;
    line-height: 35px;">How Eon Clinics Can Help</h4>
            <p>At EON, we strive to treat all of our patients how they deserve to be treated: with the BEST care possible. Are you weighing the pros and cons of a local clinic versus a corporate dental implant chain? At EON Clinics, we offer the best of both! We’re family owned and operated, and we have the most locations in Chicagoland. Here are some of the things you can expect at our clinics…</p>
            <p class="more" style="padding-left: 0;"><a href="<?php echo get_option('home'); ?>/types-of-implants">Learn more</a></p>
         </div>
      </div>
   </div>
</div>
</div>
<div class="section" id="candidate">
   <div class="container">
      <div class="row no-gutters">
         <div class="text col-lg-6">
	           <h3>Are you a good candidate?</h3>
	           <div class="inner">
            <p>The first step to finding out whether or not you are a candidate for dental implants is to come into our office for a complimentary consultation. During your initial meeting, our doctors and treatment coordinators will listen to your concerns and goals for treatment, and together, determine if dental implants are the best option for you. </p>
	           </div>
            <p class="more" style="padding-left: 0;"><a href="<?php echo get_option('home'); ?>/types-of-implants">Schedule a Free Consultation</a></p>
         </div>
         <div class="text col-lg-6">
          
         </div>
      </div>
   </div>
</div>

<div class="section" id="articles">
   <div class="container">
	   <h3 class="section-title">Recommended Articles</h3>
	  
      <div class="row no-gutters">
         <div class="item col-lg-4 d-flex flex-column justify-content-between">
            <div>
	            <img src="/wp-content/uploads/2019/05/service3-1-new.png">
                <h4>Types of Dental<br>Implants</h4>
            </div>
            <p class="more"><a href="<?php echo get_option('home'); ?>/eon-clinics-right-choice">Learn More</a></p>
         </div>
         <div class="item col-lg-4 d-flex flex-column justify-content-between">
            <div>
	            <img src="/wp-content/uploads/2019/05/benefits.png">
                <h4>Benefits of Dental<br>Implants</h4>
            </div>
            <p class="more"><a href="<?php echo get_option('home'); ?>/eon-clinics-right-choice">Learn More</a></p>
         </div>
         <div class="item col-lg-4 d-flex flex-column justify-content-between">
            <div>
               <img src="/wp-content/uploads/2019/05/iStock-186574985.png">
               <h4>Cost of Dental<br>Implants</h4>
             
            </div>
            <p class="more"><a href="<?php echo get_option('home'); ?>/eon-clinics-right-choice">Learn More</a></p>
         </div>
      </div>
   </div>
</div>

<?php get_footer(); ?>
