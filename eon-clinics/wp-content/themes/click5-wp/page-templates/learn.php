<?php
   /**
    * @package WordPress
    * @subpackage click5_WP_Theme
    */
   /*
	   Template Name: Learn More
	*/
   get_header(); ?> 
<div class="hero-sub d-flex align-items-end" <?php echo check_background(); ?> >
   <div class="container">
      <div class="row">
         <div class="col-lg-12 item">
            <h1<?php echo update_h1(); ?>><?php the_title(); ?></h1>
         </div>
      </div>
   </div>
</div>
<!--<div class="section" id="heading">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 item">
	         <div class="text-center">
            <h2>How We Can Help You?</h2>
	         </div>
         </div>
      </div>
   </div>
</div>-->
<div class="section" id="faqbox">
   <div class="container">
	   <div class="text-center">
            <h2>How We Can Help You?</h2>
	         </div>
      <div class="row no-gutters">
         <div class="col-lg-12" >
            <div class="row">
               <div class="col-md-4 mx-auto">
                  <div class="text-center">
                     <div class="card">
                        <div class="card-body" style="background: #5caa9f;">
                           <h3 class="card-title">Implant Information</h3>
							<img src="/wp-content/uploads/2019/05/WhiteDentalImplant.png" style="color:white;" alt="Implant Information Icon">
                           <p class="more" style="padding-left: 0px;" ><a href="/dental-implant-faqs/implant-information">Learn More</a></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 mx-auto">
                  <div class="text-center">
                     <div class="card">
                        <div class="card-body" style="background: #3b5ca9;">
                           <h3 class="card-title">Insurance</h3>
							<img src="/wp-content/uploads/2019/05/InsuranceIcon.png" alt="Implant Information Icon">
                           <p class="more" style="padding-left: 0px;" ><a href="/dental-implant-faqs/insurance">Learn More</a></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 mx-auto">
                  <div class="text-center">
                     <div class="card">
                       <div class="card-body" style="background: #443e9b;">
                           <h3 class="card-title">Surgery</h3>
						   <img src="/wp-content/uploads/2019/05/SurgeryTools.png" alt="Implant Information Icon">
                           <p class="more" style="padding-left: 0px;"><a href="/dental-implant-faqs/surgery">Learn More</a></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 mx-auto">
                  <div class="text-center">
                     <div class="card">
                        <div class="card-body" style="background: #ecbe44;">
                           <h3 class="card-title">Pre-Surgical</h3>
							<img src="/wp-content/uploads/2019/05/PreIcon.png" alt="Implant Information Icon">
                           <p class="more" style="padding-left: 0px;" ><a href="/dental-implant-faqs/pre-surgical">Learn More</a></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 mx-auto">
                  <div class="text-center">
                     <div class="card">
                        <div class="card-body" style="background: #e98f66;">
                           <h3 class="card-title">Post-Surgical</h3>
                           <img src="/wp-content/uploads/2019/05/PostIcon.png" alt="Implant Information Icon">
							<!--<img src="/wp-content/uploads/2019/05/Recoveryicon.png" alt="Implant Information Icon">-->
                           <p class="more" style="padding-left: 0px;" ><a href="/dental-implant-faqs/post-surgical">Learn More</a></p>
                        </div>
                     </div>
                                      </div>
               </div>
               <div class="col-md-4 mx-auto">
                  <div class="text-center">
                     <div class="card">
                        <div class="card-body" style="background: #4f84db;">
                           <h3 class="card-title">Recovery</h3>
							<img src="/wp-content/uploads/2019/05/first-aid-kit-outline.png" alt="Implant Information Icon">
                           <p class="more" style="padding-left: 0px;" ><a href="/dental-implant-faqs/recovery">Learn More</a></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="entry">
               <?php the_content(); ?>
            </div>
            <?php endwhile; endif; ?>
         </div>
      </div>
   </div>
</div>


<div class="section" id="faq_book">
   <div class="container">
      <div class="row no-gutters">
         <div class="col-lg-7" >
	        <h3>Researching Your Options?</h3>
	        <div class="book_strong">
	        <strong>Download this informative e-book that will guide you through the ins and outs of dental implants</strong>
	        </div>
	        <?php echo do_shortcode('[contact-form-7 id="515" title="FAQ Form"]') ?>        
         </div>
         <div class="col-lg-5" >
	          <div class="text-center">
	        <img src="/wp-content/uploads/2019/04/E-Book-icon-1.png">
	          </div>
         </div>
      </div>
   </div>
</div>

<?php get_footer(); ?>
