<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

/*
Template Name: Questions
*/

get_header(); ?>

<div class="hero-sub schedule d-flex align-items-end">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 item d-flex align-items-center">
                <h1<?php echo update_h1(); ?>>Have Questions?<br>We Have Answers!</h1>
            </div>
            <div class="col-lg-6 item d-flex align-items-center">
                <p>Dental implants are a life-changing solution, yet are also a big decision.<br>We understand this and we're here to answer any questions you may have.</p>
            </div>
        </div>
    </div> 
</div>    

<div class="section" id="schedule">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   <div class="entry">
                        <?php the_content(); ?>
                   </div>
                <?php endwhile; endif; ?>
                
            </div>

            <div class="col-lg-6 form-wrapper">
                <div class="item">
                    <?php echo do_shortcode('[contact-form-7 id="561" title="Questions"]'); ?>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="section" id="locations">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Consultations are available at any one of our five locations:</h3>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Hoffman Estates, IL</strong></p>
                    <p>Located in St. Alexius Medical Center close to Schaumburg</p>
                </div>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Munster, IN</strong></p>
                    <p>Right on the Illinois/Indiana border near Lansing and Hammond</p>
                </div>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Skokie, IL</strong></p>
                    <p>Near Westfield Old Orchard, accessible by Pace Bus</p>
                </div>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Westmont, IL</strong></p>
                    <p>On Fairview Ave near the Downers Grove town line</p>
                </div>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Waukesha, WI</strong></p>
                    <p>Near the 94/W Bluemound Rd interchange not far from Brookfield</p>
                </div>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>