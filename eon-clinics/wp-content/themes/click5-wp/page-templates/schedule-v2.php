<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

/*
Template Name: Schedule V2
*/

get_header(); ?>

<div class="hero-sub schedule d-flex align-items-end">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item align-items-center text-center">
                <h1 id="schedule_h1" <?php echo update_h1(); ?>>Schedule Your Free Consultation<br><strong>In 3 Easy Steps</strong></h1>
            </div>
        </div>
		<div class="row">
			<img src="wp-content/uploads/2019/08/ssl-secure-encryption.png" style="width:120px; height: 39px; margin: 0 auto;"/>
		</div>
    </div> 
</div>    

<div class="section v2" id="schedule">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="offset-lg-2 col-lg-8 form-wrapper">
                <div class="item">
                    <?php echo do_shortcode('[contact-form-7 id="123" title="Schedule"]'); ?>
					<?php //echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- <div class="section" id="locations">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Consultations are available at any one of our five locations:</h3>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Hoffman Estates, IL</strong></p>
                    <p>Located in St. Alexius Medical Center close to Schaumburg</p>
                </div>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Munster, IN</strong></p>
                    <p>Right on the Illinois/Indiana border near Lansing and Hammond</p>
                </div>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Skokie, IL</strong></p>
                    <p>Near Westfield Old Orchard, accessible by Pace Bus</p>
                </div>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Westmont, IL</strong></p>
                    <p>On Fairview Ave near the Downers Grove town line</p>
                </div>
            </div>

            <div class="item col-md-6 col-lg-4 d-flex">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div>
                    <p><strong>Waukesha, WI</strong></p>
                    <p>Near the 94/W Bluemound Rd interchange not far from Brookfield</p>
                </div>
            </div>

        </div>
    </div>
</div> -->

<?php //get_footer(); ?>
