<?php
/**
 * @package WordPress
 * @subpackage click5_WP_Theme
 */

get_header(); ?>

<div class="hero-sub d-flex align-items-end" style="background-image: url('/wp-content/uploads/2019/05/sub-hero-couple.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item">
                <h1>Page Not Found</h1>
            </div>
        </div>
    </div> 
</div>       

<div class="section" id="subpage-content">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-9" id="content-sub">

            <p>Apologies, but the page you requested could not be found. Perhaps sitemap will help.</p>
			
            <?php echo ddsg_create_sitemap(); ?>
                
            </div>

            <div class="col-lg-3" id="sidebar-area">
                <?php get_sidebar(); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>
