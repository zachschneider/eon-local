<?php

namespace Go_Live_Update_URLS\Pro\WP_CLI;

use \WP_CLI\ExitException;
use \WP_CLI\Utils;

/**
 * WP_CLI command for Go Live Update URLS
 *
 * @author OnPoint Plugins
 * @since  2.6.0
 */
class Go_Live {
	const COMMAND = 'go-live';
	const NAME    = __CLASS__;


	/**
	 * Updates all the old URLs in the database with a new URL.
	 *
	 * ## OPTIONS
	 *
	 * <old>
	 * : The old URL to replace.
	 *
	 * <new>
	 * : The new URL to replace the old URL with.
	 *
	 * [--tables=<tables>]
	 * : Optional CSV list of tables to update (defaults to all).
	 * Not to be used in conjunction with --sections.
	 *
	 * [--sections=<sections>]
	 * : Optional CSV list of sections to update (defaults to all).
	 * comments,custom,network,options,posts,terms,user
	 * Not to be used in conjunction with --tables.
	 *
	 * ## EXAMPLES
	 *     wp go-live update http://stage.domain.com https://domain.com
	 *     wp go-live update http://stage.domain.com https://domain.com --tables=wp_options,wp_postmeta
	 *     wp go-live update http://stage.domain.com https://domain.com --sections=users,network
	 *
	 * @param array $urls  - [<old url>,<new url>].
	 * @param array $flags - Optional parameters.
	 *
	 * @throws ExitException If does not have all required arguments will exit.
	 *
	 * @return void
	 */
	public function update( $urls, $flags ) {
		if ( empty( $urls[0] ) ) {
			\WP_CLI::error( __( 'You must specify an old URL', 'go-live-update-urls' ) );
		}
		if ( empty( $urls[1] ) ) {
			\WP_CLI::error( __( 'You must specify a new URL', 'go-live-update-urls' ) );
		}

		// No need to do extra work because we can specify sections or tables here.
		remove_filter( 'go-live-update-urls/database/update-tables', [
			\Go_Live_Update_URLS_Pro_Core::instance(),
			'swap_tables',
		] );

		$sections = $this->get_from_csv( $flags, 'sections' );
		$tables   = $this->get_from_csv( $flags, 'tables' );
		$all      = false;

		if ( empty( $tables ) && ! empty( $sections ) ) {
			$tables = $this->get_tables_from_sections( $sections );
		}

		if ( empty( $tables ) ) {
			$all    = true;
			$tables = \Go_Live_Update_Urls_Database::instance()->get_all_table_names();
		}

		list( $old, $new ) = $urls;

		/* translators: <Old URL><New URL>  */
		\WP_CLI::log( sprintf( _x( 'Updating any %1$s URLs to %2$s...', '{<old url>}{<new url>}', 'go-live-update-urls' ), $old, $new ) );

		if ( \Go_Live_Update_Urls_Database::instance()->update_the_database( $old, $new, $tables ) ) {
			if ( $all ) {
				\WP_CLI::success( __( 'All URLs in the database have been updated!', 'go-live-update-urls' ) );
			} else {
				\WP_CLI::success( __( 'All URLs in the following tables have been updated!', 'go-live-update-urls' ) );
				\WP_CLI::line( implode( ', ', $tables ) );
			}
		} else {
			\WP_CLI::error( __( 'Something went wrong, unable to update the URLs!', 'go-live-update-urls' ) );
		}

	}


	/**
	 * Get the list of tables from any provided valid sections.
	 * If all the provided sections don't exist this will error out.
	 *
	 * @param array $sections - List of provided sections.
	 *
	 * @throws ExitException - Exit if no valid sections.
	 *
	 * @return array
	 */
	private function get_tables_from_sections( $sections ) {
		$available = array_keys( \Go_Live_Update_URLS_Pro_Checkboxes::instance()->get_available_sections() );
		$tables    = \Go_Live_Update_URLS_Pro_Checkboxes::instance()->swap_tables( array_intersect( $sections, $available ) );

		if ( empty( $tables ) ) {
			/* translators: <CSV list of sections>  */
			\WP_CLI::error( sprintf( _x( 'Invalid sections provided! Options are %1$s.', '{sections}', 'go-live-update-urls' ), implode( ', ', $available ) ) );
		}

		return $tables;
	}


	/**
	 * Extra a value from a flag and turn it into a clean array.
	 *
	 * @param array  $flags - All passed flags.
	 * @param string $flag  - The flag we need.
	 *
	 * @return array
	 */
	private function get_from_csv( $flags, $flag ) {
		$value = Utils\get_flag_value( $flags, $flag );
		if ( null === $value ) {
			return [];
		}

		return array_map( 'trim', explode( ',', $value ) );

	}
}
