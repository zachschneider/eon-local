<?php
/**
* Plugin Name: Custom Code
* Plugin URI: http://ecreateinfotech.com
* Description: 
* Version: 1.0.0
* Author: Ecreate Infotech
* Author URI: http://ecreateinfotech.com
* License: GPL2
*/

add_filter( 'gform_field_value_utm_source', 'ec_custom_populate_utm_source' );
function ec_custom_populate_utm_source( $value ) {
	$val = '';
	if(isset($_COOKIE['utm_source']) && trim($_COOKIE['utm_source'])!='') {
		$val = trim($_COOKIE['utm_source']);
	}
	return $val;
}

add_filter( 'gform_field_value_utm_medium', 'ec_custom_populate_utm_medium' );
function ec_custom_populate_utm_medium( $value ) {
    $val = '';
    if(isset($_COOKIE['utm_medium']) && trim($_COOKIE['utm_medium'])!='') {
		$val = trim($_COOKIE['utm_medium']);
	}
	return $val;
}

add_filter( 'gform_field_value_keyword', 'ec_custom_populate_keyword' );
function ec_custom_populate_keyword( $value ) {
    $val = '';
    if(isset($_COOKIE['keyword']) && trim($_COOKIE['keyword'])!='') {
		$val = trim($_COOKIE['keyword']);
	}
	return $val;
}

add_filter( 'gform_field_value_gclid', 'ec_custom_populate_gclid' );
function ec_custom_populate_gclid( $value ) {
    $val = '';
    if(isset($_COOKIE['gclid']) && trim($_COOKIE['gclid'])!='') {
		$val = trim($_COOKIE['gclid']);
	}
	return $val;
}

add_action('init','ec_save_custom_cookie');

function ec_save_custom_cookie() {
	if(isset($_GET['utm_source'])) {
		setcookie('utm_source', trim($_GET['utm_source']), time() + 900 , "/");
	}
	if(isset($_GET['utm_medium'])) {
		setcookie('utm_medium', trim($_GET['utm_medium']), time() + 900 , "/");
	}
	if(isset($_GET['keyword'])) {
		setcookie('keyword', trim($_GET['keyword']), time() + 900 , "/");
	}
	if(isset($_GET['gclid'])) {
		setcookie('gclid', trim($_GET['gclid']), time() + 900 , "/");
	}
}