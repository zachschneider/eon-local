// When page load, if cookie exists, close the banner


document.addEventListener("DOMContentLoaded", function(e){
    if(sessionStorage.getItem('closeBanner')){
        document.getElementById('header-banner-close').parentNode.style.display = 'none';
    }
});


window.addEventListener('load', function(e){
    //When close icon is clicked, close the banner and create session storage
    document.getElementById('header-banner-close-button').addEventListener('click', function(e) {
        e.preventDefault();
        document.getElementById('header-banner-close').parentNode.style.display = 'none';
        sessionStorage.setItem('closeBanner', '1');
    }, false);
});




