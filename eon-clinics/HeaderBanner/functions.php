<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

//Header banner Scripts #Codex Creative
function header_banner_scripts() {
	//One function is in the head and the other on the footer
	//To avoid flickering
	wp_enqueue_style( 'header-banner-style', get_stylesheet_directory_uri() . '/HeaderBanner/header-banner-style.css');
	wp_enqueue_script( 'eonclinics-site-banner-control', get_stylesheet_directory_uri() . '/HeaderBanner/header-banner.js', array(), false, false );
}
add_action('wp_enqueue_scripts', 'header_banner_scripts');

/**
 * Load the Optimizely X script *after jQuery*
 */
function init_optimizely_after_jquery() {
    wp_enqueue_script( 'optimizely-x', '//cdn.optimizely.com/js/16342820700.js', array( 'jquery' ), '', false );
}
add_action( 'wp_enqueue_scripts', 'init_optimizely_after_jquery' );

//======================================================================
// CUSTOM DASHBOARD
//======================================================================
// ADMIN FOOTER TEXT
function remove_footer_admin () {
    echo "Divi Child Theme";
}

add_filter('admin_footer_text', 'remove_footer_admin');

/* Gravity Forms Read Only Class */

// update '1' to the ID of your form
add_filter( 'gform_pre_render_5', 'add_readonly_script' );
function add_readonly_script( $form ) {
    ?>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            /* apply only to a input with a class of gf_readonly */
            jQuery("li.gf_readonly input").attr("readonly","readonly");
        });
    </script>

    <?php
    return $form;
}



// CUSTOM VIDEO MODULE
function DS_Custom_Modules() {
    if(class_exists("ET_Builder_Module")){
    include("video-custom.php");
    }
    // if ( ! class_exists('ET_Builder_Module') ) {
    //     return;
    // }

    // include( 'custom-modules/cfwpm' );

    // $cfwpm = new Custom_ET_Builder_Module_Video();

    // remove_shortcode( 'et_pb_video' );

    // add_shortcode( 'et_pb_video_2', array($cfwpm, '_shortcode_callback') );

}
function Prep_DS_Custom_Modules(){
    global $pagenow;

   $is_admin = is_admin();
    $action_hook = $is_admin ? 'wp_loaded' : 'wp';
    $required_admin_pages = array( 'edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php', 'export.php' ); // list of admin pages where we need to load builder files
    $specific_filter_pages = array( 'edit.php', 'admin.php', 'edit-tags.php' );
    $is_edit_library_page = 'edit.php' === $pagenow && isset( $_GET['post_type'] ) && 'et_pb_layout' === $_GET['post_type'];
    $is_role_editor_page = 'admin.php' === $pagenow && isset( $_GET['page'] ) && 'et_divi_role_editor' === $_GET['page'];
    $is_import_page = 'admin.php' === $pagenow && isset( $_GET['import'] ) && 'wordpress' === $_GET['import'];
    $is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset( $_GET['taxonomy'] ) && 'layout_category' === $_GET['taxonomy'];

   if ( ! $is_admin || ( $is_admin && in_array( $pagenow, $required_admin_pages ) && ( ! in_array( $pagenow, $specific_filter_pages ) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page ) ) ) {
    add_action($action_hook, 'DS_Custom_Modules', 9789);
    }
   }
   Prep_DS_Custom_Modules();



// CUSTOM POST TYPE - Doctors
function create_posttype() {
	register_post_type( 'doctors',
    array(
      'labels' => array(
        'name' => __( 'Doctors' ),
        'singular_name' => __( 'Doctor' )
      ),
      'public' => true,
      'has_archive' => false,
      'rewrite' => array('slug' => 'doctors','with_front' => false),
      'taxonomies' => array( 'category' ),
      'menu_position' => 5,
      'supports' => array(
          'title',
          'thumbnail',
					'editor',
					'excerpt' )
    )
  );
	}
add_action( 'init', 'create_posttype' );

flush_rewrite_rules( false );

// END
