This code is for a site-wide closable banner that is displayed bellow the top nav bar.
In this directory there's files that correspond to `header.php` and `functions.php`.
The code aditions are here:


This code needs to be added to `functions.php` inside the current theme
``` php
//Header banner Scripts #CodexCreative
function header_banner_scripts() {
	wp_enqueue_style( 'header-banner-style', get_stylesheet_directory_uri() . '/HeaderBanner/header-banner-style.css');
	wp_enqueue_script( 'eonclinics-site-banner-control', get_stylesheet_directory_uri() . '/HeaderBanner/site-banner-header.js', array(), false, false );
}
add_action('wp_enqueue_scripts', 'header_banner_scripts');
```



This is the markup for the header, it needs to be added to `header.php` inside the current theme:
```html
<!-- Heeader Banner #CodexCreative -->
		<div id="header-banner" class="header-banner">
			<p id="header-banner-close">
				<a id="header-banner-close-button">x</a>
			</p>
			<strong>Coronavirus Preparedness:</strong> We have decided to temporarily close all five of our clinics to ensure the safety and peace of mind of our patients, staff, and communities.
			<br>
			<a href="https://eoncstage.wpengine.com/schedule-your-free-consultation-1" id="header-banner-link">
				Please schedule a consultation for when we reopen in May or any time after!
			</a>
		</div>
```



Styling is added in `\css\header-banner-style.css` directory inside the current theme:
```css
.header-banner{
    text-align: center;
	background-color: #FFD37A;
	padding: 20px;
	padding-top: 10px;
    transition-duration: .2s;
}

#header-banner-close{
	text-align: end;
}

#header-banner-close-button{
    cursor: pointer;
    text-decoration: none;
}

#header-banner-link{
	text-decoration: underline;
    color: black;
}
```

JS functionality is inside `\js\header-banner.js`
It uses session storage to hide the banner if the user clicked the close.
This is effective until the browser tab closes.
``` js
document.addEventListener("DOMContentLoaded", function(e){
    if(sessionStorage.getItem('closeBanner')){
        document.getElementById('header-banner-close').parentNode.style.display = 'none';
    }
});

window.addEventListener('load', function(e){
    //When close icon is clicked, close the banner and create session storage
    document.getElementById('header-banner-close-button').addEventListener('click', function(e) {
        e.preventDefault();
        document.getElementById('header-banner-close').parentNode.style.display = 'none';
        sessionStorage.setItem('closeBanner', '1');
    }, false);
});
```
